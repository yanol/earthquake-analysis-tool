#!/usr/bin/env python

from Tkinter import *
import tkFileDialog

import array
import numpy
import subprocess
from PIL import Image, ImageTk
import matplotlib.path as mplPath
import matplotlib.pyplot as plt
import numpy
import collections
import matplotlib.pyplot
import pylab
import math
import os.path

from classes.equake import Equake
from classes.tplot import TernaryPlot
from classes.datahandler import dataHandler
from classes.region import Region

################################################################################################## GUI #####################################
############################################################################################### SUBREGION ###################################################################################################        
#############################################################################################################################################################################################################

class subregion_GUI(Frame):
    def __init__(self, parent, top, class_reg, name):
	Frame.__init__(self, parent)
	self.parent = parent
	self.name = name
	self.top = top
	self.class_reg = class_reg
	self.pack(fill=BOTH, expand=1)
	self.angle = DoubleVar(self)
	self.option = None
	self.minmax = self.get_minmax()

	self.parent.title('EAT (Earthquake Analysis Tool) v1.0 - subregion: '+self.name+".")

	#drawing variables
	self.x_0, self.y_0, self.x, self.y, self.line = 0, 0, 0, 0, None

	#menu bar code
	self.menubar = Menu(self)

	menu = Menu(self.menubar, tearoff=0)
	self.menubar.add_cascade(label='Statistics', menu=menu)
	menu.add_command(label='Create triangle plot',command=self.create_Triangle)
	menu.add_command(label='Plot section (gmt and .png)',command=self.create_Section)
	menu.add_command(label='Create 3d plot',command= lambda: self.create_3dplot(1))
	menu.add_command(label='Create 3d surface plot',command= lambda: self.create_3dplot(2))
	menu.add_command(label='Create 3d contour plot',command= lambda: self.create_3dplot(3))
	menu.add_command(label='Calculate azimuth',command=self.create_azimuth)
	menu.add_command(label='Calculate b plot and show',command=self.create_bplot)
	menu.add_command(label='Change depth to median',command= lambda: self.change_depth(1))
	menu.add_command(label='Change depth to average',command= lambda: self.change_depth(2))
	menu.add_command(label='Filter between 0 and 70',command= lambda: self.filter_between([[0,20],[20,50],[50,70],[70,120],[120,300]]))
	menu.add_command(label='Export region base',command=self.create_plot)	
	menu.add_command(label='Run 100 iterations',command=self.create_analysis)	

	self.parent.config(menu=self.menubar)

	self._canvas_init()

    def create_analysis(self):
	self.class_reg.self_analyse()

    def _canvas_init(self):
	self.img = Image.open(self.name+".jpg")
	w, h = self.img.size
	self.parent.geometry(str(w)+"x"+str(h))

        self.img_tk = ImageTk.PhotoImage(self.img)

	self.canvas = Canvas(self, width=w, height=h)
	self.canvas.pack(side='top', fill='both', expand=True)

        self.canvas.create_image(0,0, anchor = 'nw', image = self.img_tk)

	self.canvas.bind("<Double-Button-1>", self.on_button_double_press)
	self.canvas.bind("<ButtonPress-1>", self.on_press_action)
        self.canvas.bind("<B1-Motion>", self.on_move_action)
        self.canvas.bind("<ButtonRelease-1>", self.on_button_release_action)

    def create_plot(self):
	add_eqs = subprocess.Popen("gmt/globetest.gmt "+str(self.minmax[0])+" "+str(self.minmax[1])+" "+str(self.minmax[2])+" "+str(self.minmax[3])+" "+self.name+"_base.ps",shell=True).wait()
	add_ps = subprocess.Popen("gmt psxy "+self.name+"_coords.xy -JM -R -O -W1.0 -K >> "+self.name+"_base.ps", shell=True).wait()
	add_average = subprocess.Popen("gmt psmeca "+self.name+".xy -R -JM -Sm6.0c/-1  -Gyellow -W1 -T0 -O -K >> "+self.name+"_base.ps", shell=True).wait()	

	add_eqs = subprocess.Popen("gmt/globetest.gmt "+str(self.minmax[0])+" "+str(self.minmax[1])+" "+str(self.minmax[2])+" "+str(self.minmax[3])+" "+self.name+"_base_eqs.ps",shell=True).wait()
	add_ps = subprocess.Popen("gmt psxy "+self.name+"_coords.xy -JM -R -O -W1.0 -K >> "+self.name+"_base_eqs.ps", shell=True).wait()
	add_average = subprocess.Popen("gmt psmeca "+self.name+"_filtered_eqs.txt -R -JM -Sm1.5c/-1  -Gblack -W1 -T0 -O -K >> "+self.name+"_base_eqs.ps", shell=True).wait()

    def filter_between(self, vals):

	for i in vals:
		minim = i[0]
		maxim = i[1]
		
		if minim > maxim:
			minim, maxim = maxim, minim
	
		self.class_reg.region_between_depths(minim, maxim)

    def create_bplot(self):
	self.class_reg.return_b_value(0)

    def change_depth(self, option):
	self.class_reg.depth = self.class_reg.return_stat_Depth(option)

    def create_azimuth(self):
	region = self.class_reg
	
	region.return_Angle()

    def create_3dplot(self, option):
	region = self.class_reg

	x_70, y_70, z_70 = [], [], []
	x_100, y_100, z_100 = [], [], []
	x_150, y_150, z_150 = [], [], []
	x_r, y_r, z_r = [], [], []
	x, y, z = [], [], []

	for e in region.earthquakes:
		x.append(e.lon)
		y.append(e.lat)
		z.append(-1.0*e.depth)
		if (e.depth < 70):
			x_70.append(e.lon)
			y_70.append(e.lat)
			z_70.append(-1.0*e.depth)
		elif (e.depth < 100):
			x_100.append(e.lon)
			y_100.append(e.lat)
			z_100.append(-1.0*e.depth)
		elif (e.depth < 150):
			x_150.append(e.lon)
			y_150.append(e.lat)
			z_150.append(-1.0*e.depth)
		elif (e.depth > 150):
			x_r.append(e.lon)
			y_r.append(e.lat)
			z_r.append(-1.0*e.depth)

	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')

	if (option != 1):

		xi = numpy.linspace(min(x), max(x))
		yi = numpy.linspace(min(y), max(y))

		X, Y = numpy.meshgrid(xi, yi)
		Z = griddata(x, y, z, xi, yi)

	if (option == 3):
		ctr = ax.contour(xi, yi, Z)
		ax.clabel(ctr, inline=1, fontsize=10)
		
		labels = ['1','2','3','4']

		ax.set_ylabel("Latitude")
		ax.set_xlabel("Longitude")
		ax.set_zlabel("Depth (km)")

		plt.savefig(self.name+"_3d_surface.png")

	if (option == 2):

		surf = ax.plot_surface(X, Y, Z, rstride=5,cstride=5,cmap=cm.jet,linewidth=1,antialiased=True)
		ax.set_zlim3d(numpy.min(Z), numpy.max(Z))
		ax.set_ylabel("Latitude")
		ax.set_xlabel("Longitude")
		ax.set_zlabel("Depth (km)")
		plt.savefig(self.name+"_contour_plot.png")

	if (option == 1):                

		ax.plot(x_70, y_70, z_70, 'o', c='y', label='<70km')
		ax.plot(x_100, y_100, z_100, 'o', c='b', label='<100km')
		ax.plot(x_150, y_150, z_150, 'o', c='r', label='<150km')
		ax.plot(x_r, y_r, z_r, 'o', c='k', label='>150km')
		ax.set_ylabel("Latitude")
		ax.set_xlabel("Longitude")
		ax.set_zlabel("Depth (km)")
		ax.legend()
		plt.savefig(self.name+"_3d_scatter.png")

	plt.show()
	plt.clf()

    def get_minmax(self):
	region = self.class_reg

	reg_coords = region.coords
	xmin, xmax, ymin, ymax = reg_coords[0][0], reg_coords[1][0], reg_coords[0][1], reg_coords[1][1]

	for i in reg_coords:
		x = i[0]
		y = i[1]	
		if (x > xmax):
			xmax = x
		if (x < xmin):
			xmin = x
		if (y > ymax):
			ymax = y
		if (y < ymin):
			ymin = y

	return [xmin, xmax, ymin, ymax]

    def convert_coords(self, coords):

	region = self.class_reg

	reg_coords = region.coords
	xmin, xmax, ymin, ymax = reg_coords[0][0], reg_coords[1][0], reg_coords[0][1], reg_coords[1][1]

	for i in reg_coords:
		x = i[0]
		y = i[1]	
		if (x > xmax):
			xmax = x
		if (x < xmin):
			xmin = x
		if (y > ymax):
			ymax = y
		if (y < ymin):
			ymin = y
		

	img_x = self.canvas.winfo_reqwidth()
	img_y = self.canvas.winfo_reqheight()
	
	#max long and lat values for the window
	x_1 = xmin
	y_1 = ymin

	x_2 = xmax
	y_2 = ymax

	c_x = (x_2 - x_1)
	c_y = (y_2 - y_1)
	

	#work out new selected coords
	new_x_1 = x_1 + ((coords[0]/img_x)*c_x)
	new_y_1 = y_2 - ((coords[1]/img_y)*c_y)
	new_x_2 = x_1 + ((coords[2]/img_x)*c_x)
	new_y_2 = y_2 - ((coords[3]/img_y)*c_y)

	return new_x_1, new_y_1, new_x_2, new_y_2

    def on_press_action(self, event):
	self.x_0 = event.x
	self.y_0 = event.y

	if (self.line != None):
		self.canvas.delete(self.line)

	self.line = self.canvas.create_line(self.x, self.y, 1, 1, fill='red', width=10, dash = (4,2))	

    def on_move_action(self, event):
	new_x = event.x
	new_y = event.y

	self.canvas.coords(self.line, self.x_0, self.y_0, new_x, new_y)
	
    def on_button_release_action(self, event):
	pass

    def create_Section(self):

	coords = self.canvas.coords(self.line)
	new_coords = self.convert_coords(coords)

	print new_coords

	region = self.class_reg
	outfile = open(region.name+"_projection.xy", 'w')
	for e in region.earthquakes:
		outfile.write(str(e.lon)+" "+str(e.lat)+" "+str(e.depth)+'\n')
	outfile.close()

	x_1, y_1, x_2, y_2 = new_coords[0], new_coords[1], new_coords[2], new_coords[3]
	print x_1, y_1, x_2, y_2
	print region.coords

	create_projection = subprocess.Popen("gmt project "+region.name+"_projection.xy -C"+str(x_1)+"/"+str(y_1)+" -E"+str(x_2)+"/"+str(y_2)+" -Fpz -Q > "+region.name+"_projection.txt",shell=True).wait()

	create_basemap = subprocess.Popen('gmt psxy '+region.name+'_projection.txt -R0/400/0/400 -JX15c/-7c -Ba50f10:"Distance along section (km)":/a50f10:"Depth (km)"::."":WSne -Sc0.3c -P > '+region.name+'_section.ps', shell=True).wait()

	x_v, y_v = [], []

	infile = open(region.name+"_projection.txt")
	for line in infile:
		splt = line.split()
		x = float(splt[0])
		y = float(splt[1])*-1.0
		x_v.append(x)
		y_v.append(y)

	plt.xlabel("Distance (km)")
	plt.ylabel("Depth (km)")
        plt.scatter(x_v, y_v)
	plt.title(self.name+" projection plot")
	plt.savefig(self.name+"_projection_plot.png")
	plt.show()
	

    def create_Triangle(self):
	region = self.class_reg
	region.compare_Stats()

    def on_button_double_press(self, event):

	infile = open(self.name+"_deltas.txt")
	for line in infile:
		if ("fault plane dip" in line):
			line_words = line.split()
			angle1 = float(line_words[3])
			angle2 = float(line_words[4])
			break
	if (self.class_reg != None):
		options = []
		options.append(angle1)
		options.append(angle2)
		print angle1, angle2
		self.new_window = Toplevel(self.parent)
		self.app = option_Menu(self.new_window, self, self.angle, options)

	else:
		print "No class value found!"

    def get_Velocity(self):
	if (self.angle != None):
		velocity = self.class_reg.calculate_Velocity(float(self.angle.get()), self.class_reg.length, self.class_reg.depth)
		lon = self.class_reg.av_coords[0]
		lat = self.class_reg.av_coords[1]
		azim = self.class_reg.strain_data[2]

		self.class_reg.return_Angle(int(self.option))

		outfile = open(self.name+"_velocity.xy", 'w')
		#DIVIDED BY 10
		print "Option is "+str(self.option)
		outfile.write(str(lon)+" "+str(lat)+" "+str(self.class_reg.angle)+" "+str(velocity))
		outfile.close()

		print velocity
	else:
		print "Angle not defined!"

    def get_Sum(self):
	
	if (self.class_reg != None):
		self.class_reg.create_Summation_file("region_1.xy")
	else:
		print "Subregion has no attached region class!"

############################################################################################### MAIN GUI SECTION OF THE CODE ################################################################################        
#############################################################################################################################################################################################################

class main_GUI(Frame):
    def __init__(self, parent, regcoords, *regions):
	Frame.__init__(self, parent)
	self.parent = parent
	self.regcoords = regcoords
	self.pack(fill=BOTH, expand=1)
	self.regions = None	
	if regions:
		self.regions = regions

	#self.parent.resizable(0,0)
	self.parent.title('EAT (Earthquake Analysis Tool) v1.0 - main region')
	self.parent.protocol("WM_DELETE_WINDOW", self.quit)

	self.x, self.y = 0,0
	self.x_0, self.y_0, self.rect = None, None, None
	self.all_rect = 0
	self.eq = None
	
	#menu bar code
	self.menubar = Menu(self)

	menu = Menu(self.menubar, tearoff=0)
	self.menubar.add_cascade(label='File', menu=menu)
	menu.add_command(label='Exit and delete all files',command=self.clear_Files)
	menu.add_command(label='Exit (leave files in directory)',command=self.quit)
	menu.add_command(label='Save',command=self.save_Regions)
	menu.add_command(label='Export all',command=self.create_Data)

	menu = Menu(self.menubar, tearoff=0)
	self.menubar.add_cascade(label='Regions', menu=menu)
	menu.add_command(label='Add region')

	self.parent.config(menu=self.menubar)

	#rotation parameters
	self.rotating = False
	self.center = None
	self.start_angle = None
	self.coords = None
	self.current_r = None

	self._canvas_init()

	if self.regions != None:
		self.initialise_Saved()

    def create_Data(self):

	coords = self.regcoords

	eq_map = False
	strain_map = False
	av_map = False
	vel_map = True
	sub_plot = False
	make_label = False
	ind_map = False
	export_char = False

	#export character
	if (export_char):
		for i in scotia.all_regions:
			if ("_0" not in i.name):
				i.compare_Stats()

	#create velocity map
	if (vel_map):
		create_ps = subprocess.Popen("gmt/globetest.gmt "+str(coords[0])+" "+str(coords[2])+" "+str(coords[1])+" "+str(coords[3])+" mainmap_vel.ps", shell=True).wait()
		for i in scotia.all_regions:
			if ("_0" not in i.name):
				add_ps = subprocess.Popen("gmt psxy "+i.name+"_coords.xy -JM -R -O -W1.0 -K >> mainmap_vel.ps", shell=True).wait()
				if (os.path.exists(i.name+"_velocity.xy")):
					add_vel = subprocess.Popen("gmt psxy "+i.name+"_velocity.xy -JM -R -SV0.2+e -W0.2,red -Gred -L -O -K >> mainmap_vel.ps", shell=True).wait()
					
	#create earthquake map - ALL
	if (eq_map):
		create_ps = subprocess.Popen("gmt/globetest.gmt "+str(coords[0])+" "+str(coords[2])+" "+str(coords[1])+" "+str(coords[3])+" mainmap_eqs.ps", shell=True).wait()
		add_eqs = subprocess.Popen("gmt/add_earthquakes.gmt mainmap_eqs.ps 0.2",shell=True).wait()
		for i in scotia.all_regions:
			if ("_0" not in i.name):
				add_ps = subprocess.Popen("gmt psxy "+i.name+"_coords.xy -JM -R -O -W1.0 -K >> mainmap_eqs.ps", shell=True).wait()
	
	#make av map
	if (av_map):
		create_ps = subprocess.Popen("gmt/globetest.gmt "+str(coords[0])+" "+str(coords[2])+" "+str(coords[1])+" "+str(coords[3])+" mainmap_average.ps", shell=True).wait()
		for i in scotia.all_regions:
			if ("_0" not in i.name):
				add_ps = subprocess.Popen("gmt psxy "+i.name+"_coords.xy -JM -R -O -W1.0 -K >> mainmap_average.ps", shell=True).wait()
				add_average = subprocess.Popen("gmt psmeca "+i.name+".xy -R -JM -Sm2.0c/-1  -Gyellow -W1 -T0 -O -K >> mainmap_average.ps", shell=True).wait()

	#plot average strain/summed tensor map - ALL
	if (make_label):
		create_ps = subprocess.Popen("gmt/globetest.gmt "+str(coords[0])+" "+str(coords[2])+" "+str(coords[1])+" "+str(coords[3])+" mainmap_average.ps", shell=True).wait()
		for i in scotia.all_regions:
			if ("_0" not in i.name):
				if ("region_5" == i.name or "region_1" == i.name or "10" in i.name):
					add_ps = subprocess.Popen("gmt psxy "+i.name+"_coords.xy -JM -R -O -W1.0 -Ggreen@60 -K >> mainmap_average.ps", shell=True).wait()
				elif ("14" in i.name or "9" in i.name):
					add_ps = subprocess.Popen("gmt psxy "+i.name+"_coords.xy -JM -R -O -W1.0 -Gred@60 -K >> mainmap_average.ps", shell=True).wait()
				elif ("8" in i.name or "region_3" == i.name or "6" in i.name):
					add_ps = subprocess.Popen("gmt psxy "+i.name+"_coords.xy -JM -R -O -W1.0 -Gorange@60 -K >> mainmap_average.ps", shell=True).wait()
				else:
					add_ps = subprocess.Popen("gmt psxy "+i.name+"_coords.xy -JM -R -O -W1.0 -Gblue@60 -K >> mainmap_average.ps", shell=True).wait()	
		for idx,i in enumerate(scotia.all_regions):
			if ("_0" not in i.name):
				add_ps = subprocess.Popen("gmt psxy "+i.name+"_coords.xy -JM -R -O -W1.0 -Gblue@60 -K >> mainmap_average.ps", shell=True).wait()

				add_text = subprocess.Popen("gmt pstext "+i.name+"_name_location.txt -JM -R -F+f4,Helvetica,white+jTL -Gblack -O -K >> mainmap_average.ps", shell=True).wait()
		add_label = subprocess.Popen('''gmt pstext <<END -JM -R -F+f12,Helvetica,white+jTL -Gblack -O -K >> mainmap_average.ps
-53 -51 Zone A
END''', shell=True).wait()
		add_label = subprocess.Popen('''gmt pstext <<END -JM -R -F+f12,Helvetica,white+jTL -Gblack -O -K >> mainmap_average.ps
-30 -52 Zone B
END''', shell=True).wait()
		add_label = subprocess.Popen('''gmt pstext <<END -JM -R -F+f12,Helvetica,white+jTL -Gblack -O -K >> mainmap_average.ps
-50 -58 Zone C
END''', shell=True).wait()
		add_label = subprocess.Popen('''gmt pstext <<END -JM -R -F+f12,Helvetica,white+jTL -Gblack -O -K >> mainmap_average.ps
-78 -58 Zone D
END''', shell=True).wait()
		
	#create individual map .ps for each subregion
	if (ind_map):
		for i in scotia.all_regions:
			if ("_0" not in i.name):
				coords = i.coords

				xmin,xmax,ymin,ymax = coords[0][0], coords[1][0], coords[0][1], coords[1][1]
				for c in coords:
					if c[0] < xmin:
						xmin = c[0]
					if c[0] > xmax:
						xmax = c[0]
					if c[1] < ymin:
						ymin = c[1]
					if c[1] > ymax:
						ymax = c[1]					

				add_eqs = subprocess.Popen("gmt/globetest.gmt "+str(xmin)+" "+str(xmax)+" "+str(ymin)+" "+str(ymax)+" "+i.name+".ps",shell=True).wait()
				add_ps = subprocess.Popen("gmt psxy "+i.name+"_coords.xy -JM -R -O -W1.0 -K >> "+i.name+".ps", shell=True).wait()
				add_eqs = subprocess.Popen("gmt/add_earthquakes.gmt "+i.name+".ps 1.0",shell=True).wait()
				
				if ("_14" in i.name):
					add_ps = subprocess.Popen("gmt psxy cross_section_coords.xy -JM -R -O -W2.0,black -Gblue@60 -K >> "+i.name+".ps", shell=True).wait()
					add_ps = subprocess.Popen("gmt psxy cross_section_coords2.xy -JM -R -O -W2.0,red -K >> "+i.name+".ps", shell=True).wait()

				#add_eqs = subprocess.Popen("ps2pdf "+i.name+".ps "+i.name+".pdf",shell=True).wait()
				#add_eqs = subprocess.Popen("pdfcrop "+i.name+".ps "+i.name+".pdf",shell=True).wait()
				#add_average = subprocess.Popen("gmt psmeca "+i.name+".xy -R -JM -Sm2.0c/-1  -Gyellow -W1 -T0 -O -K >> "+i.name+".ps", shell=True).wait()
				#pr = 0.7552
				#size = 200
				#if i.strain_data != None:
				#		size = pr/i.strain_data[0]	
				#add_strain = subprocess.Popen("gmt psvelo "+i.name+"_strain.xy -JM -W1 -Sx"+str(size)+" -R -O -K >> "+i.name+".ps", shell=True).wait() 

	#create strain map - NORMALISED - ALL
	if (strain_map):
		create_ps = subprocess.Popen("gmt/globetest.gmt "+str(coords[0])+" "+str(coords[2])+" "+str(coords[1])+" "+str(coords[3])+" mainmap_strain.ps", shell=True).wait()
		for i in scotia.all_regions:
			if ("_0" not in i.name):
				add_ps = subprocess.Popen("gmt psxy "+i.name+"_coords.xy -JM -R -O -W1.0 -K >> mainmap_strain.ps", shell=True).wait()
				pr = 0.7552
				size = 200
				if i.strain_data != None:
					size = pr/i.strain_data[1]	
					add_strain1 = subprocess.Popen("gmt psvelo "+i.name+"_strain_1.xy -JM -W1,blue -Sx"+str(size)+" -R -O -K >> mainmap_strain.ps", shell=True).wait() 
					add_strain2 = subprocess.Popen("gmt psvelo "+i.name+"_strain_2.xy -JM -W1,red -Sx"+str(size)+" -R -O -K >> mainmap_strain.ps", shell=True).wait() 
	
	#plot sub regions
	if (sub_plot):
		for i in scotia.all_regions:
			if ("_0" not in i.name):
				for x in i.subfilter:
					coords = x.coords

					xmin,xmax,ymin,ymax = coords[0][0], coords[1][0], coords[0][1], coords[1][1]
					for c in coords:
						if c[0] < xmin:
							xmin = c[0]
						if c[0] > xmax:
							xmax = c[0]
						if c[1] < ymin:
							ymin = c[1]
						if c[1] > ymax:
							ymax = c[1]

					add_eqs = subprocess.Popen("gmt/globetest.gmt "+str(xmin)+" "+str(xmax)+" "+str(ymin)+" "+str(ymax)+" "+x.name+"_strain.ps",shell=True).wait()
					add_ps = subprocess.Popen("gmt psxy "+x.name+"_coords.xy -JM -R -O -W1.0 -K >> "+x.name+"_strain.ps", shell=True).wait()
					add_eqs = subprocess.Popen("gmt psmeca "+x.name+"_earthquakes.xy -R -JM -Sm0.8c/-1  -Glightblue -W1 -T0 -O -K >> "+x.name+"_strain.ps", shell=True).wait()
					add_average = subprocess.Popen("gmt psmeca "+x.name+".xy -R -JM -Sm3.0c/-1  -Gyellow -W1 -T0 -O -K >> "+x.name+"_strain.ps", shell=True).wait()
					if ("region_14_filtered_0_20" not in x.name):
						add_strain1 = subprocess.Popen("gmt psvelo "+x.name+"_strain_1.xy -JM -W2,blue -Sx40000 -R -O -K >> "+x.name+"_strain.ps", shell=True).wait() 
						add_strain2 = subprocess.Popen("gmt psvelo "+x.name+"_strain_2.xy -JM -W2,red -Sx40000 -R -O -K >> "+x.name+"_strain.ps", shell=True).wait() 
					else:	
						add_strain1 = subprocess.Popen("gmt psvelo "+x.name+"_strain_1.xy -JM -W2,blue -Sx10000 -R -O -K >> "+x.name+"_strain.ps", shell=True).wait() 
						add_strain2 = subprocess.Popen("gmt psvelo "+x.name+"_strain_2.xy -JM -W2,red -Sx10000 -R -O -K >> "+x.name+"_strain.ps", shell=True).wait() 
				
			

    def clear_Files(self):
	for i in scotia.all_regions:
		name = i.name
	
		delete_ps = subprocess.Popen("rm "+tag+".xy "+tag+".jpg "+tag+"_deltas.txt "+tag+"_coords.xy "+tag+"_bplot.png "+tag+"_histogram.png "+tag+"_strain.xy", shell=True).wait()
		if (delete_ps == 0):
			print "Cleared files!"
		else:
			print "Couldn't delete subregion files - QUIT program."

	self.quit()

    def initialise_Saved(self):
	
	for region in self.regions[0]:
		region_coords = []
		for i in xrange(0, len(region), 2):
			x = region[i]
			y = region[i+1]
			region_coords.append(x)
			region_coords.append(y)

		self.all_rect = self.all_rect + 1

		self.rect = self.canvas.create_polygon(region_coords[0], region_coords[1], region_coords[2], region_coords[3], region_coords[4], region_coords[5], region_coords[6], region_coords[7], outline = 'red', fill='gray', stipple='gray12', tag = 'region_'+str(self.all_rect))

		coords = self.convert_coords(region_coords)
		self.create_Region(coords, 'region_'+str(self.all_rect))

		if (self.rect != None):
			self.canvas.tag_bind(self.rect, "<ButtonPress-3>", self.on_rect_click) 
			self.canvas.tag_bind(self.rect, "<Double-Button-1>", self.double_rect_click)
			self.canvas.tag_bind(self.rect, "<ButtonPress-1>", self.begin_rotate)
			self.canvas.tag_bind(self.rect, "<B1-Motion>", self.rectangle_rotate)
			self.rect = None
	self.parent.deiconify()

    def save_Regions(self):

	name = tkFileDialog.asksaveasfile(mode='w', defaultextension=".txt", initialfile='savefile.txt', filetypes=[('all files','.*'), ('text files', '.txt')])
	if name is None:
		print "Please input a file name!"
		return

	i = scotia.all_regions[0]
	name.write(str(i.name)+" ")
	for t in i.coords:
		name.write(" ".join(str(h) for h in t)+" ")
	name.write("\n")

	for i in self.canvas.find_all():
		name_string = self.canvas.gettags(i)
		if (len(name_string) > 0):
			name.write(str(name_string[0])+" "+' '.join(str(j) for j in self.canvas.coords(i))+"\n")	

	name.close()

    def _canvas_init(self):
	self.img = Image.open('main_region.jpg')
	w, h = self.img.size
	root.geometry(str(w)+"x"+str(h))
	self.center = [w/2., h/2.]

        self.img_tk = ImageTk.PhotoImage(self.img)

	self.canvas = Canvas(self, width=w, height=h)
	self.canvas.pack(side='top', fill='both', expand=True)

        self.canvas.create_image(0,0, anchor = 'nw', image = self.img_tk)	

	if self.regions == None:
		self.parent.deiconify()		

	self.canvas.bind("<ButtonPress-1>", self.button_press_action)
        self.canvas.bind("<B1-Motion>", self.move_action)
        self.canvas.bind("<ButtonRelease-1>", self.button_release_action)

    def convert_coords(self, coords):

	img_x = self.canvas.winfo_reqwidth()
	img_y = self.canvas.winfo_reqheight()
	
	#max long and lat values for the window

	x_1 = self.regcoords[0]
	y_1 = self.regcoords[1]

	x_2 = self.regcoords[2]
	y_2 = self.regcoords[3]

	c_x = (x_2 - x_1)
	c_y = (y_2 - y_1)

	#work out new selected coords

	new_x_1 = x_1 + ((coords[0]/img_x)*c_x)
	new_y_1 = y_2 - ((coords[1]/img_y)*c_y)
	new_x_2 = x_1 + ((coords[4]/img_x)*c_x)
	new_y_2 = y_2 - ((coords[5]/img_y)*c_y)

	new_x_3 = x_1 + ((coords[6]/img_x)*c_x)
	new_y_3 = y_2 - ((coords[7]/img_y)*c_y)
	new_x_4 = x_1 + ((coords[2]/img_x)*c_x)
	new_y_4 = y_2 - ((coords[3]/img_y)*c_y)

	return new_x_1, new_y_1, new_x_3, new_y_3, new_x_2, new_y_2, new_x_4, new_y_4

	#name_string = name

    def create_Region(self, coords, name):

	name_string = name

	scotia.create_Region(coords[0], coords[1], coords[2], coords[3], coords[4], coords[5], coords[6], coords[7], name_string)

	if (os.path.exists(name_string+".jpg")):
		return	

	#assign random value to min/max values which we then override
	xmin, xmax, ymin, ymax = coords[0], coords[2], coords[1], coords[3]

	for i in xrange(0, len(coords), 2):
		x = coords[i]
		y = coords[i+1]	
		if (x > xmax):
			xmax = x
		if (x < xmin):
			xmin = x
		if (y > ymax):
			ymax = y
		if (y < ymin):
			ymin = y
					
	create_ps = subprocess.Popen("gmt/EAR.gmt"+" "+str(xmin)+" "+str(xmax)+" "+str(ymin)+" "+str(ymax)+" "+name_string+".xy", shell=True).wait()
	add_ps = subprocess.Popen("gmt psxy "+name_string+"_coords.xy -JQ -R -O -W1.0 -K >> EAR.ps", shell=True).wait()

	if (create_ps == 0):
		create_png = subprocess.Popen("convert -density 400 EAR.ps -trim -resize '900' -quality 100 "+name_string+".jpg", shell=True).wait()
		if (create_png == 0):
			print "Generated basemap .png for subregion."

    def button_press_action(self, event):
        self.x_0, self.y_0 = event.x, event.y
        
    def move_action(self, event):
	if (self.rotating == False):
		x_1, y_1 = event.x, event.y
		if self.rect == None:
			self.all_rect = self.all_rect + 1

			self.rect = self.canvas.create_polygon(self.x, self.y, 1, 1, outline = 'red', fill='gray', stipple='gray12', tag = 'region_'+str(self.all_rect))
			#print "Region name is from TKinter as: "+str(self.rect.gettags))
		self.canvas.coords(self.rect, self.x_0, self.y_0, self.x_0, y_1, x_1, y_1, x_1, self.y_0)
        
    def button_release_action(self, event):
	if (self.rect != None):
		self.canvas.tag_bind(self.rect, "<ButtonPress-3>", self.on_rect_click) 
		self.canvas.tag_bind(self.rect, "<Double-Button-1>", self.double_rect_click)
		self.canvas.tag_bind(self.rect, "<ButtonPress-1>", self.begin_rotate)
		self.canvas.tag_bind(self.rect, "<B1-Motion>", self.rectangle_rotate)
		self.rect = None
	if(self.rotating == True):
		self.rotating = False
		self.center = None
		self.start_angle = None
		self.coords = None
		self.current_r = None
	#print str(len(self.all_rect))

    def get_Angle(self, event):
	dx = self.canvas.canvasx(event.x) - self.center[0]
	dy = self.canvas.canvasy(event.y) - self.center[1]

	complx = abs(complex(dx, dy))

	if (complx != 0):
		return complex(dx, dy) / complx

    def begin_rotate(self, event):
	x_c, y_c = event.x, event.y
	object = self.canvas.find_closest(x_c, y_c)
	rect = object[0]

	if (rect != None):

		coords = self.canvas.coords(rect)
		self.coords = [(coords[0], coords[1]),(coords[2], coords[3]), (coords[4], coords[5]), (coords[6], coords[7])]
		#find center of our polygon using the canvas points
		self.center = [(coords[2]+coords[4]+coords[0]+coords[6])/4, (coords[1]+coords[3]+coords[5]+coords[7])/4]
		#get the start angle in terms of i and j
		self.start_angle = self.get_Angle(event)
		self.rotating = True
		self.current_r = rect
		print self.start_angle

    def rectangle_rotate(self, event):
	
	if (self.rotating == True):
		current_angle = self.get_Angle(event) / self.start_angle
		diff = complex(self.center[0], self.center[1])
	
		draw_xy = []
	
		for x, y in self.coords:
			v = current_angle * (complex(x, y) - diff) + diff
			draw_xy.append(v.real)
			draw_xy.append(v.imag)
		#self.canvas.coords(self.current_r, *draw_xy)
		self.canvas.coords(self.current_r, *draw_xy)
	
    def double_rect_click(self, event):
	x_c, y_c = event.x, event.y
	object = self.canvas.find_closest(x_c, y_c)
	rect = object[0]

	if (rect):

		tag = self.canvas.gettags(rect)[0]

		i = 0

		for e in scotia.all_regions:
			if str(e.name) == str(tag):
				self.eq = e

		if (os.path.exists(tag+".xy") and os.path.exists(tag+".jpg")):
			self.new_window = Toplevel(self.parent)
			self.app = subregion_GUI(self.new_window, self.parent, self.eq, tag)
		else:

			coords = self.canvas.coords(rect)
			print coords
			new_coords = self.convert_coords(coords)
			self.create_Region(new_coords, tag)

			i = 0

			for e in scotia.all_regions:
				if str(e.name) == str(tag):
					self.eq = e
		
			#print "Tag of object is: "+str(tag)
			#self.canvas.delete(object[0])
			self.new_window = Toplevel(self.parent)
			self.app = subregion_GUI(self.new_window, self.parent, self.eq, tag)

    def on_rect_click(self, event):

	x_c, y_c = event.x, event.y
	object = self.canvas.find_closest(x_c, y_c)

	print "Number of regions: "+str(len(scotia.all_regions))+"."

	#print event.type, event.widget
	#print "Clicked."
	rect = object[0]	
	if (rect):
		tag = self.canvas.gettags(rect)[0]
		print "Tag of object is: "+str(tag)

		i = 0

		for e in scotia.all_regions:
			if str(e.name) == str(tag):
				scotia.all_regions.pop(i)
			i = i + 1

		self.canvas.delete(rect)
		delete_ps = subprocess.Popen("rm "+tag+".xy "+tag+".jpg "+tag+"_deltas.txt "+tag+"_coords.xy "+tag+"_bplot.png "+tag+"_histogram.png "+tag+"_strain.xy", shell=True).wait()
		if (delete_ps == 0):
			pass
		else:
			print "Couldn't delete subregion files - QUIT program."
		self.all_rect = self.all_rect - 1
		print "Number of regions: "+str(len(scotia.all_regions))+"."

############################################################################################### SELECTION GUI ###############################################################################################        
#############################################################################################################################################################################################################

class selectGUI(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.parent = parent
	self.pack(fill=BOTH, expand=1)

        self.parent.resizable(0,0)
	self.parent.title('EAT (Earthquake Analysis Tool) v1.0')
        
	self.x, self.y = 0,0
	self.regcoords = []

	self.img = Image.open('images/newbasemap.png')
	w, h = self.img.size

	#menu bar code
	self.menubar = Menu(self)
	menu = Menu(self.menubar, tearoff=0)
	self.menubar.add_cascade(label='File', menu=menu)
	menu.add_command(label='Load region data file', command=self.load_File)
	menu.add_command(label='Exit', command=self.quit)

	self.parent.config(menu=self.menubar)
        
        self.canvas = Canvas(self, width=w, height=h)
        self.canvas.pack(side='top', fill='both', expand=True)
        self.canvas.bind("<ButtonPress-1>", self.button_press_action)
        self.canvas.bind("<B1-Motion>", self.move_action)
        self.canvas.bind("<ButtonRelease-1>", self.button_release_action)
        
        w = self.canvas.winfo_reqwidth()
        
        f = Frame(self, height=32, width=w)
        f.pack_propagate(0)
        f.pack()
        
        self.select_button = Button(f, text = 'Select region', command = self.initiate_region)
        self.select_button.pack(fill=BOTH, expand = 1)
        
        self.rect = None
        
        self.x_0 = None
        self.y_0 = None
        
        self._canvas_init()

    def _canvas_init(self):
        
        self.img_tk = ImageTk.PhotoImage(self.img)
        #print "open"
        self.canvas.create_image(0,0, anchor = 'nw', image = self.img_tk)

    def load_File(self):

	name = tkFileDialog.askopenfilename(defaultextension=".txt", initialfile='savefile.txt', filetypes=[('all files','.*'), ('text files', '.txt')])
	if name is None:
		print "Please input a file name!"
		return
	
	infile = open(name)

	main = []
	regions = []
	
	for line in infile:
		if "region_0" in line:
			for j in line.split(" "):
				try:
					main.append(float(j))
				except ValueError:
					pass
		else:
			temp = []
			for j in line.split(" "):
				try:
					temp.append(float(j))
				except ValueError:
					pass
			regions.append(temp)

	infile.close()	

	self.parent.withdraw()
	self.destroy()
	
	self.generate_Files([main[0], main[5], main[2], main[3]])
	self.app = main_GUI(self.parent, [main[0], main[5], main[2], main[3]], regions)
			

    def convert_coords(self, coords):

	img_x = self.canvas.winfo_reqwidth()
	img_y = self.canvas.winfo_reqheight()
	
	x_1 = -180 + (coords[0]/img_x * 360)
	y_2 = 90 - (coords[1]/img_y * 180)
	x_2 = -180 + (coords[2]/img_x * 360)
	y_1 = 90 - (coords[3]/img_y * 180)

	self.regcoords = [x_1, y_1, x_2, y_2]

	print x_1, y_1, x_2, y_2

	return [x_1, y_1, x_2, y_2]

    def generate_Files(self, coords):

	x_1, y_1, x_2, y_2 = coords[0], coords[1], coords[2], coords[3]

	scotia.create_Region(x_1, y_2, x_2, y_2, x_2, y_1, x_1, y_1, "region_0")
	scotia.filter_by_Depth(0, 10, "region_0_to_10.xy")
	scotia.filter_by_Depth(10, 15, "region_10_to_15.xy")
	scotia.filter_by_Depth(15, 20, "region_15_to_20.xy")
	scotia.filter_by_Depth(20, 50, "region_20_to_50.xy")
	scotia.filter_by_Depth(50, 300, "region_50_to_300.xy")
	scotia.filter_by_Depth(300, 20000, "region_300_to_20000.xy")

	create_ps = subprocess.Popen("gmt/EAR.gmt"+" "+str(x_1)+" "+str(x_2)+" "+str(y_1)+" "+str(y_2), shell=True).wait()
	
	#stdout, stderr = create_ps.communicate()
	#output = create_ps.returncode

	if (create_ps == 0):
		create_png = subprocess.Popen("convert -density 400 EAR.ps -trim -resize '1500' -quality 100 main_region.jpg", shell=True).wait()
		if (create_png == 0):
			print "Generated basemap .png for main region."	

    def initiate_region(self):
	
	if self.rect != None:

		self.parent.withdraw()

		coords = self.canvas.coords(self.rect)
		
		coords = self.convert_coords(coords)
		self.generate_Files(coords)

		#self.canvas.delete(ALL)
		for child in self.winfo_children():
			child.destroy()
		self.destroy()

		#self.new_window = Toplevel(self.parent)
		self.app = main_GUI(self.parent, self.regcoords)

	else:
		self.destroy()
        
    def button_press_action(self, event):
        self.x_0 = event.x
        self.y_0 = event.y
        
	if self.rect != None:
		self.canvas.delete(self.rect)
	
	self.rect = self.canvas.create_rectangle(self.x, self.y, 1, 1, outline = 'black', dash = (4,2))
        
    def move_action(self, event):
        x_1, y_1 = event.x, event.y
        
        self.canvas.coords(self.rect, self.x_0, self.y_0, x_1, y_1)
        
    def button_release_action(self, event):
        pass

############################################################################################### OPTION GUI  #################################################################################################        
#############################################################################################################################################################################################################

class option_Menu(Frame):
    def __init__(self, parent, top, variable, options):
	Frame.__init__(self, parent)
	self.parent = parent
	self.top = top
	self.var = variable
	self.options = options

	self.pack(fill=BOTH, expand=1)

	self.parent.title('Option menu')

	self.var.set(self.options[0])

	self.choice = apply(OptionMenu, (self, self.var) + tuple(self.options))
	self.choice.pack()

	self.button = Button(self, text='Select', command=self.get_Value)
	self.button.pack()

    def get_Value(self):
		#print "Var. is: "+str(float(self.var.get()))

		if (abs(float(self.var.get()) - float(self.options[0])) < 0.10):
			self.top.option = 0
		else:
			self.top.option = 1

		self.top.get_Velocity()
		self.parent.destroy()
	

############################################################################################### MAIN LOOP  ##################################################################################################        
#############################################################################################################################################################################################################
    
if __name__ == '__main__':

	scotia = dataHandler('data/all_data_added.txt')
	root = Tk()
	app = selectGUI(root)
	root.mainloop()
