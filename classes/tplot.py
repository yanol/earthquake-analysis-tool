import matplotlib.pyplot as plt
import numpy
from pylab import figure

#Ternary plot class #class edited from dave's http://stackoverflow.com/questions/701429/library-tool-for-drawing-ternary-triangle-plots

class TernaryPlot:
    def __init__(self, data, labels=('Strike-slip', 'Normal', 'Thrust')):
	self.labels = labels
        self.edge_args = {'color':'black','linewidth':2}
        self.fig_args = {'figsize':(8,8),'facecolor':'white','edgecolor':'white'}
        self.data = data
        self.a = 0.3
        self.size = 200
        self.basis = numpy.array(                 
                        [[math.cos(2*i*numpy.pi/3 + 90*numpy.pi/180),
                        math.sin(2*i*numpy.pi/3 + 90*numpy.pi/180)]       
                        for i in range(3)]
                    )        
        self.dot_data = numpy.dot(self.data, self.basis)      
        self.fig = figure(**self.fig_args)
        self.ax = self.fig.add_subplot(111)        
        self.ax.set_xticks(())
        self.ax.set_yticks(())
        self.ax.set_frame_on(False)
        self.color = "black"
        self.labels = labels
        
        self._init_labels()
        
    def _init_labels(self):
        for i, n in enumerate(self.labels):
            x = self.basis[i,0]
            y = self.basis[i,1]
            self.ax.text(x*1.1, y*1.1, n, horizontalalignment='center', verticalalignment='center')
        
    def save(self, name):
	self.fig.savefig(name)

    def plot(self):
        
        self.ax.plot(
        [self.basis[i,0] for i in range(3) + [0,]],
        [self.basis[i,1] for i in range(3) + [0,]],
        **self.edge_args
        )
        
        self.ax.scatter(self.dot_data[:,0], self.dot_data[:,1], 'x', s = self.size, alpha = self.a, color=self.color)
	plt.show()
    
    def add_data(self, data, *arg):
        if arg:
            self.color = arg
        
        self.data = data
        self.dot_data = dot(self.data, self.basis)
        self.plot()
        
        
    def change_alpha(alp):
        self.a = alp
    
    def change_size(size):
        self.size = size
