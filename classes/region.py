from equake import Equake
import matplotlib.path as mplPath
import array
import numpy
import subprocess
from PIL import Image, ImageTk
import matplotlib.path as mplPath
import matplotlib.pyplot as plt
import numpy
import collections
import matplotlib.pyplot
import pylab
import math
import os.path
from decimal import Decimal
import array
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.mlab import griddata
from pylab import figure
import random

alpha = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S"]
options_values = [2,1,2,2,1,1,1,2,2,1,1,1,2,1]

############################################################################################### REGION CODE #################################################################################################        
#############################################################################################################################################################################################################

class Region:
    
    def __init__(self, name, coords, earthquakes):
        self.name = name
        self.coords = coords
        self.earthquakes = earthquakes
        self.sum_list = self.do_Summation()
        self.depth = self.return_stat_Depth(1)
        self.length = self.return_Length()
        self.width = self.return_Width()
	self.av_coords = self.return_average_coords()
	self.create_Summation_file(self.name)

	self.strain_data = self.create_strain_file()

	self.dip = self.return_Dip()
	self.velocity = self.calculate_Velocity(self.length, self.depth, self.dip)
	

	self.b_value = self.return_b_value(0)
	self.dip = self.return_Dip()
	self.velocity = self.calculate_Velocity(self.length, self.depth, self.dip)

    def return_all_values(self):
	return [self.dip, self.velocity, self.sum_list]

    def self_analyse(self):
	
	l = self.length
	w = self.width 
	outfile = open(self.name+"_iterations_velocity.xy", 'w')
	c = math.sqrt(l**2 + w**2)

	print self.name+" values are: "+str(self.depth)+" "+str(l)+" "+str(c)

	outfile.close()	

	length = self.length
	width = self.width
	area = length*width

	outfile = open(self.name+"_iterations_strain.xy", 'w')

	for i in range(0,100):
		temp = []
		for e in self.earthquakes:
			if (random.randint(0,10) > 2):
				temp.append(e)

		length = random.randint(int(l),int(c))
		depth = random.randint(int(self.depth)-5,int(self.depth)+5)

		Region("region_3", self.coords, temp)		

		#area_random = random.randint(int(area/2.0), int(area))
		#depth = self.depth
		#random.randint(int(depth-2), int(depth+2))
		#strain = self.calculate_Strain(area_random, depth)
		#outfile.write(str(strain)+" ")

	outfile.close()

    def write_vel(self):
	outfile = open("all_new_vels.xy", 'a')
	outfile.write(str(self.velocity)+'\n')
	outfile.close()

    def create_Files(self):
	m0sum = self.return_m0_sum()
	stra = self.strain
	area = self.return_Area()
	
	infile = open (self.name+"_deltas.txt")

	split_name = self.name.split("_")
	num = int(split_name[1])-1

	opt = options_values[num]

	x = self.av_coords[0]
	y = self.av_coords[1]

	for line in infile:
		if "fault plane dip" in line:
			splt = line.split()
			delt = float(splt[2+int(opt)])	

	outfile = open("all_data_stored.txt", 'a')
	outfile.write(self.name+" "+str(m0sum)+" "+str(stra)+" "+str(area)+" "+str(delt)+" "+str(x)+" "+str(y)+" "+str(self.b_value)+'\n')
	#outfile.close()

    def region_between_depths(self, minim, maxim):
	if minim > maxim:
		minim, maxim = maxim, minim

	temp = []	
	for e in self.earthquakes:
		if e.depth > minim:
			temp.append(e)
		if e.depth > maxim:
			break

	name_file = self.name+"_filtered_"+str(minim)+"_"+str(maxim)
	outfile = open(name_file+"_earthquakes.xy", 'w')

	for e in temp:
		outfile.write(e.return_Equake())

	new_r = Region(name_file, self.coords, temp)
	self.subfilter.append(new_r)

    def write_Angle(self):
	splt = self.name.split("_")
	print splt
	
	num = int(splt[1])

	angle = self.return_Angle(int(options_values[num-1]))

	outfile = open(self.name+"_velocity.xy", 'w')
	outfile.write(str(self.av_coords[0])+" "+str(self.av_coords[1])+" "+str(angle)+" 1.20")
	outfile.close()

    def return_Dip(self):
	splt = self.name.split("_")
	delt = None
	print splt
	
	num = int(splt[1])

	val = options_values[num-1]

	infile = open (self.name+"_deltas.txt")

	for line in infile:
		if "fault plane dip" in line:
			splt = line.split()
			delt = float(splt[2+int(val)])
	infile.close()	
	
	return delt

    def return_Angle(self, option):

	delt, lambd, phi = None, None, None

	infile = open (self.name+"_deltas.txt")
	for line in infile:
		if "fault plane dip" in line:
			splt = line.split()
			delt = float(splt[2+int(option)])
		if "fault plane strike" in line:
			splt = line.split()
			lambd = float(splt[2+int(option)])
		if "fault plane rake" in line:
			splt = line.split()
			phi = float(splt[2+int(option)])

	print delt, lambd, phi

	if (delt != None and lambd != None and phi != None):
		delta_r = math.radians(delt)		
		lambda_r = math.radians(lambd)
		phi_r = math.radians(phi)		

		y = math.cos(lambda_r)*math.cos(phi_r) + math.sin(lambda_r)*math.cos(delta_r)*math.sin(phi_r)
		x = -math.cos(lambda_r)*math.sin(phi_r) + math.sin(lambda_r)*math.cos(delta_r)*math.cos(phi_r)
	else:
		print "Angles read incorrectly!"

	angle = None

	if (x > 0 and y > 0):
		angle = math.degrees(math.atan(x/y))
	if (x > 0 and y < 0):
		angle = 90 + math.degrees(math.atan(y/x))
	if (x < 0 and y < 0):
		angle = 180 + math.degrees(math.atan(x/y))
	if (x < 0 and y > 0):
		angle = 270 + math.degrees(math.atan(y/x))

	if (angle != None):
		print "Azimuth is calculated to be: "+str(angle)+"."
		if ("region_2" == self.name or "region_3" == self.name or "region_4" == self.name or "region_5" == self.name or "region_7" == self.name or "region_12" == self.name or "region_13" == self.name):
			angle = angle + 180
		self.angle = angle
		return angle
	else:
		print "Angle is None!"

    def return_average_coords(self):

	lon_total = 0
	lat_total = 0

	for i in self.coords:
		#c = coords[i]
		lon_total = lon_total + i[0]
		lat_total = lat_total + i[1]

	lon = lon_total/len(self.coords)
	lat = lat_total/len(self.coords)

	return [lon, lat]

    def compare_Stats(self):

	comp = None

	x_v = []
	y_v = []
	x_v.append(0)
	y_v.append(0)

	p_ax, i_ax, t_ax = None, None, None
	outfile = open(self.name+"_character.txt", 'w')

	def dtor(number):
            return number * math.pi/180

	all_e_ch = []
	
	for e in self.earthquakes:
		string_m = str(e.mrr)+" "+str(e.mtt)+" "+str(e.mpp)+" "+str(e.mrt)+" "+str(e.mrp)+" "+str(e.mtp)
		create_mttk = subprocess.Popen("bash/get_region_delta.sh "+str(e.iexp)+" "+string_m+" eq.txt", shell=True).wait()

		if (os.path.exists("eq.txt") and create_mttk == 0):
			infile = open("eq.txt")
			for line in infile:
				if "pit axes" in line:
					splt = line.split()
					p_ax = float(splt[2])
					i_ax = float(splt[3])
					t_ax = float(splt[4])
			if (p_ax != None and i_ax != None and t_ax != None):
				e.dips = [p_ax, i_ax, t_ax]
				print e.dips
		
		if (e.dips):

			#outfile.write(str(i_ax)+" "+str(p_ax)+" "+str(t_ax)+"\n")

			pax = math.radians(p_ax)
			iax = math.radians(i_ax)
			tax = math.radians(t_ax)



			a = math.sin(iax)**2
			b = math.sin(pax)**2
			c = math.sin(tax)**2
			#print a,b,c
			#p = numpy.array([a,b,c])
			#all_e_ch.append(p)



			string_a = str(a)+" "+str(b)+" "+str(c)+"\n"
			outfile.write(string_a)
			
			#num = array.array([a, b, c])
			#x_v.append(num)

			#ctr = math.radians(35.26)
			#pax, iax, tax = 35.43333, 35.43333, 35.43333

			#phi = math.atan(math.sin(tax)/math.sin(pax)) - math.radians(45)

			#x = (math.cos(iax)*math.sin(phi))/(math.sin(ctr)*math.sin(iax) + math.cos(ctr)*math.sin(iax)*math.cos(phi))
			#y = (math.cos(ctr)*math.sin(iax) - math.sin(ctr)*math.cos(iax)*math.cos(phi)) / (math.sin(ctr)*math.sin(iax) + math.cos(ctr)*math.sin(iax)*math.cos(phi))


			#if (x > -50 and x < 50 and y > - 50 and y < 50):
			#	x_v.append(x)
			#	y_v.append(y)

			#print x, y 

	#outfile = open("character.txt", 'w')
	#for i in x_v:
	#	outfile.write(str(i)+",")
	#outfile.close()

	#outfile = open("coords_y.txt", 'w')
	#for i in y_v:
	#	outfile.write(str(i)+",")
	#outfile.close()
	
	#plt.xlabel("Character")
	#plt.ylabel("Character")
        #plt.scatter(x_v, y_v)
	#plt.title(self.name+" triangle plot")
	#plt.show()	
	#plt.savefig(self.name+"_triangle_plot.png")
	#plt.clf()


	### USE THIS NOW ###
	#data = numpy.vstack((all_e_ch))
	#h = TernaryPlot(data)
	#h.plot()
	#h.save(self.name+"_triangle_plot.png")
	outfile.close()
	delete_mttk = subprocess.Popen("rm eq.txt", shell=True).wait()
        
    def return_m0_sum(self):
        
        return self.sum_list[len(self.sum_list)-1]
    
    def return_Sums(self):
        
        return self.sum_list[0:len(self.sum_list)-1]

    def create_strain_file(self):
	
	if (os.path.exists(self.name+"_strain.xy") and os.path.exists(self.name+"_strain_1.xy") and os.path.exists(self.name+"_strain_2.xy")):
		#print "Exists!"
		infile = open(self.name+"_strain.xy")
		for line in infile:
			splt = line.split()
			eig1 = float(splt[2])
			eig2 = float(splt[3])
			azim = float(splt[4])
		infile.close()
		return [eig1, eig2, azim]

	shear = 3*10**10
	t = 39
	denom = self.length*self.width*2*shear*t

	eig1, eig2, azim, expo = None, None, None, None

	infile = open(self.name+"_deltas.txt")
	for line in infile:
		splt = line.split()
		if len(splt) == 3:
			eig1 = float(splt[0])
			eig2 = float(splt[2])
		if len(splt) == 1 and azim == None:
			azim = float(splt[0])
			continue
		if len(splt) == 1 and azim != None and expo == None:
			expo = float(splt[0])
	infile.close()

	#print eig1, eig2, azim, expo

	if (expo != None and denom != None):
		if (eig1 != None):
			eig1 = (eig1*10**expo)/denom
		if (eig2 != None):
			eig2 = (eig2*10**expo)/denom

	#self.strain_data = [eig1, eig2, azim, expo]
	#print self.strain_data

	lon_t, lat_t = 0, 0

	for i in self.coords:
		lon_t += i[0]
		lat_t += i[1]

	lon = lon_t/len(self.coords)
	lat = lat_t/len(self.coords)

	if (eig1 != None and eig2 != None and azim != None and expo != None):
		outfile = open(self.name+"_strain.xy", 'w')
		outfile.write(str(lon)+" "+str(lat)+" "+str("{:f}".format(eig1))+" "+str("{:f}".format(eig2))+" "+str(azim))
		outfile.close()

		outfile = open(self.name+"_strain_1.xy", 'w')
		outfile.write(str(lon)+" "+str(lat)+" 0 "+str("{:f}".format(eig2))+" "+str(azim))
		outfile.close()

		outfile = open(self.name+"_strain_2.xy", 'w')
		outfile.write(str(lon)+" "+str(lat)+" "+str("{:f}".format(eig1))+" 0 "+str(azim))
		outfile.close()

	return  [eig1, eig2, azim]

    def create_Summation_file(self, *name):

	#if (os.path.exists(self.name+"_strain.xy")):
	#	print "Exists!"
	#	pass
	#else:
	#print "Creating file!"
	#self.create_strain_file()

	#if (os.path.exists(self.name+"_bplot.png")):
	#	pass
	#else:


	##self.return_b_value(0)
	##
	##if (os.path.exists(self.name+"_histogram.png")):
	##	pass
	##else:
	##	self.return_histogram()

	
	#self.return_histogram()
	#self.return_b_value(0)

	#if (os.path.exists(self.name+".xy") and os.path.exists(self.name+"_deltas.txt") and os.path.exists(self.name+"_coords.xy")):
	#	return

	

	if name:
		typefile = name[0]+".xy"
	else:
		typefile = raw_input('What do you want to save the data as [.txt/.XY]:')
	
	coords = self.coords
	
	lon_total = 0
	lat_total = 0

	outf = open(self.name+"_coords.xy", 'w')
	
	first = None

	for i in coords:
		#c = coords[i]
		lon_total = lon_total + i[0]
		lat_total = lat_total + i[1]
		write_s = str(i[0])+" "+str(i[1])+"\n"
		outf.write(write_s)
		if first == None:
			first = write_s
	outf.write(first)
		
	outf.close()

	lon = lon_total/len(coords)
	lat = lat_total/len(coords)

	self.av_coords = [lon, lat]

	#exp = 10**(int(math.log10(self.sum_list[0])))
	#exp = int(math.log10(self.sum_list[0]))
	#print "Exp is: "+str(math.log10(exp))
	#print self.sum_list[0]
	a = Decimal(self.sum_list[0]).normalize().as_tuple()
	
	exp = 10**(len(a.digits)-1 + int(a.exponent))
	#print exp

	m1 = self.sum_list[0]/exp
	m2 = self.sum_list[1]/exp
	m3 = self.sum_list[2]/exp
	m4 = self.sum_list[3]/exp
	m5 = self.sum_list[4]/exp
	m6 = self.sum_list[5]/exp

	#print m1,m2,m3,m4,m5,m6

	outfile = open(typefile, 'w')
	string_m = str(m1)+" "+str(m2)+" "+str(m3)+" "+str(m4)+" "+str(m5)+" "+str(m6)

	string_sum = str(lon)+" "+str(lat)+" "+str(self.depth)+" "+string_m+" "+str(math.log10(exp))+" X Y"
	
	outfile.write(string_sum)

	outfile.close()

	#print "get_region_delta.sh "+str(math.log10(exp))+" "+string_m+" "+self.name+"_deltas.txt"
	create_mttk = subprocess.Popen("bash/get_region_delta.sh "+str(int(math.log10(exp)))+" "+string_m+" "+self.name+"_deltas.txt", shell=True).wait()
    
    def do_Summation(self):
        
        totalmrr, totalmtt, totalmpp, totalmrt, totalmrp, totalmtp, totalm0 = 0, 0, 0, 0, 0, 0, 0
        
        for e in self.earthquakes:
            expo = 10**int(e.iexp)
            
            totalmrr += (10**-7 * expo * (e.mrr))
            totalmtt += (10**-7 * expo * (e.mtt))
            totalmpp += (10**-7 * expo * (e.mpp))
            totalmrt += (10**-7 * expo * (e.mrt))
            totalmrp += (10**-7 * expo * (e.mrp))
            totalmtp += (10**-7 * expo * (e.mtp))
            
            spl = e.m0.split('E+')
            val = float(spl[0])
            exponen = 10**float(spl[1])
            
            m0_new = (10**-7)*val*exponen
            
            totalm0 += m0_new
        
        return numpy.asanyarray([totalmrr, totalmtt, totalmpp, totalmrt, totalmrp, totalmtp, totalm0])
    
    def output_File(self, *name):
        
        if name:
            typefile = name[0]
        else:
            typefile = raw_input('What do you want to save the data as [.txt/.XY]:')
        
        outfile = open(typefile, 'w')
        
        for e in self.earthquakes:
           outfile.write(e.return_Equake())

        outfile.close()

        return typefile
    
    def return_stat_Depth(self, *arg):
        
        if arg:
            method = arg[0]
        else:
            method = float(input("1 for average of earthquakes, 2 for median."))

        all_e = len(self.earthquakes)
        
        if (method == 1):

            total = 0

            for e in self.earthquakes:
                total += e.depth
            
            if (all_e == 0):
                #print "No earthquake data found, setting depth as 0!"
                return 0
            
            #print "Average is: "+str((total)/all_e)+"."
            return (total/all_e)
        
        elif (method == 2):
            
            if (all_e % 2 == 0):
                return self.earthquakes[all_e/2].depth
            else:
                half = all_e/2.0
                return (self.earthquakes[int(half-0.5)].depth+self.earthquakes[int(half+0.5)].depth)/2
	
	elif (method == 3):
		minim, maxim = None

		for e in self.earthquakes:
			if (minim == None):
				minim = e.depth
			if (maxim == None):
				maxim = e.depth
			if e.depth > maxim:
				maxim = e.depth
			if e.depth < minim:
				minim = e.depth
############################################################################################### REGION CODE #################################################################################################        
#############################################################################################################################################################################################################

class Region:
    
    def __init__(self, name, coords, earthquakes):
        self.name = name
        self.coords = coords
        self.earthquakes = earthquakes
        self.sum_list = self.do_Summation()
        self.depth = self.return_stat_Depth(1)
        self.length = self.return_Length()
        self.width = self.return_Width()
	self.av_coords = self.return_average_coords()
	self.create_Summation_file(self.name)

	self.strain_data = self.create_strain_file()

	self.dip = self.return_Dip()
	self.velocity = self.calculate_Velocity(self.length, self.depth, self.dip)
	


	#self.deviation = self.compare_Stats
	#self.return_histogram()	
	#self.strain = self.calculate_Strain(self.length*self.width, self.depth)
	#self.angle = None
	#self.subfilter = []
	self.b_value = self.return_b_value(0)
	#self.create_Files()
	#self.write_Angle()
	self.dip = self.return_Dip()
	#print self.length
	self.velocity = self.calculate_Velocity(self.length, self.depth, self.dip)
	#self.write_vel()
	#self.self_analyse()

    def return_all_values(self):
	return [self.dip, self.velocity, self.sum_list]

    def self_analyse(self):
	
	l = self.length
	w = self.width 
	outfile = open(self.name+"_iterations_velocity.xy", 'w')
	c = math.sqrt(l**2 + w**2)

	print self.name+" values are: "+str(self.depth)+" "+str(l)+" "+str(c)

	#print c
	
	#for i in range(0,1000):
	#
	#	length = random.randint(int(l),int(c))
	#	depth = random.randint(int(self.depth)-5,int(self.depth)+5)
	#	#print self.depth, l, c
	#	vel = self.calculate_Velocity(length, depth, self.dip)
	#	outfile.write(str(vel)+'\n')
	
	#outfile = open(self.name+"_iterations.xy", 'ab+')
	#outfile.write(vel)
	outfile.close()	

	#for i in range(0,1000):
	length = self.length
	width = self.width
	area = length*width
	#depth = self.depth

	outfile = open(self.name+"_iterations_strain.xy", 'w')

	for i in range(0,100):
		temp = []
		for e in self.earthquakes:
			if (random.randint(0,10) > 2):
				temp.append(e)

		length = random.randint(int(l),int(c))
		depth = random.randint(int(self.depth)-5,int(self.depth)+5)

		Region("region_3", self.coords, temp)		

		#area_random = random.randint(int(area/2.0), int(area))
		#depth = self.depth
		#random.randint(int(depth-2), int(depth+2))
		#strain = self.calculate_Strain(area_random, depth)
		#outfile.write(str(strain)+" ")

	outfile.close()

    def write_vel(self):
	outfile = open("all_new_vels.xy", 'a')
	outfile.write(str(self.velocity)+'\n')
	outfile.close()

    def create_Files(self):
	m0sum = self.return_m0_sum()
	stra = self.strain
	area = self.return_Area()
	
	infile = open (self.name+"_deltas.txt")

	split_name = self.name.split("_")
	num = int(split_name[1])-1

	opt = options_values[num]

	x = self.av_coords[0]
	y = self.av_coords[1]

	for line in infile:
		if "fault plane dip" in line:
			splt = line.split()
			delt = float(splt[2+int(opt)])	

	outfile = open("all_data_stored.txt", 'a')
	outfile.write(self.name+" "+str(m0sum)+" "+str(stra)+" "+str(area)+" "+str(delt)+" "+str(x)+" "+str(y)+" "+str(self.b_value)+'\n')
	#outfile.close()

    def region_between_depths(self, minim, maxim):
	if minim > maxim:
		minim, maxim = maxim, minim

	temp = []	
	for e in self.earthquakes:
		if e.depth > minim:
			temp.append(e)
		if e.depth > maxim:
			break

	name_file = self.name+"_filtered_"+str(minim)+"_"+str(maxim)
	outfile = open(name_file+"_earthquakes.xy", 'w')

	for e in temp:
		outfile.write(e.return_Equake())

	new_r = Region(name_file, self.coords, temp)
	self.subfilter.append(new_r)

    def write_Angle(self):
	splt = self.name.split("_")
	print splt
	
	num = int(splt[1])

	angle = self.return_Angle(int(options_values[num-1]))

	outfile = open(self.name+"_velocity.xy", 'w')
	outfile.write(str(self.av_coords[0])+" "+str(self.av_coords[1])+" "+str(angle)+" 1.20")
	outfile.close()

    def return_Dip(self):
	splt = self.name.split("_")
	delt = None
	print splt
	
	num = int(splt[1])

	val = options_values[num-1]

	infile = open (self.name+"_deltas.txt")

	for line in infile:
		if "fault plane dip" in line:
			splt = line.split()
			delt = float(splt[2+int(val)])
	infile.close()	
	
	return delt

    def return_Angle(self, option):

	delt, lambd, phi = None, None, None

	infile = open (self.name+"_deltas.txt")
	for line in infile:
		if "fault plane dip" in line:
			splt = line.split()
			delt = float(splt[2+int(option)])
		if "fault plane strike" in line:
			splt = line.split()
			lambd = float(splt[2+int(option)])
		if "fault plane rake" in line:
			splt = line.split()
			phi = float(splt[2+int(option)])

	print delt, lambd, phi

	if (delt != None and lambd != None and phi != None):
		delta_r = math.radians(delt)		
		lambda_r = math.radians(lambd)
		phi_r = math.radians(phi)		

		y = math.cos(lambda_r)*math.cos(phi_r) + math.sin(lambda_r)*math.cos(delta_r)*math.sin(phi_r)
		x = -math.cos(lambda_r)*math.sin(phi_r) + math.sin(lambda_r)*math.cos(delta_r)*math.cos(phi_r)
	else:
		print "Angles read incorrectly!"

	angle = None

	if (x > 0 and y > 0):
		angle = math.degrees(math.atan(x/y))
	if (x > 0 and y < 0):
		angle = 90 + math.degrees(math.atan(y/x))
	if (x < 0 and y < 0):
		angle = 180 + math.degrees(math.atan(x/y))
	if (x < 0 and y > 0):
		angle = 270 + math.degrees(math.atan(y/x))

	if (angle != None):
		print "Azimuth is calculated to be: "+str(angle)+"."
		if ("region_2" == self.name or "region_3" == self.name or "region_4" == self.name or "region_5" == self.name or "region_7" == self.name or "region_12" == self.name or "region_13" == self.name):
			angle = angle + 180
		self.angle = angle
		return angle
	else:
		print "Angle is None!"

    def return_average_coords(self):

	lon_total = 0
	lat_total = 0

	for i in self.coords:
		#c = coords[i]
		lon_total = lon_total + i[0]
		lat_total = lat_total + i[1]

	lon = lon_total/len(self.coords)
	lat = lat_total/len(self.coords)

	return [lon, lat]

    def compare_Stats(self):

	comp = None

	x_v = []
	y_v = []
	x_v.append(0)
	y_v.append(0)

	p_ax, i_ax, t_ax = None, None, None
	outfile = open(self.name+"_character.txt", 'w')

	def dtor(number):
            return number * math.pi/180

	all_e_ch = []
	
	for e in self.earthquakes:
		string_m = str(e.mrr)+" "+str(e.mtt)+" "+str(e.mpp)+" "+str(e.mrt)+" "+str(e.mrp)+" "+str(e.mtp)
		create_mttk = subprocess.Popen("bash/get_region_delta.sh "+str(e.iexp)+" "+string_m+" eq.txt", shell=True).wait()

		if (os.path.exists("eq.txt") and create_mttk == 0):
			infile = open("eq.txt")
			for line in infile:
				if "pit axes" in line:
					splt = line.split()
					p_ax = float(splt[2])
					i_ax = float(splt[3])
					t_ax = float(splt[4])
			if (p_ax != None and i_ax != None and t_ax != None):
				e.dips = [p_ax, i_ax, t_ax]
				print e.dips
		
		if (e.dips):

			#outfile.write(str(i_ax)+" "+str(p_ax)+" "+str(t_ax)+"\n")

			pax = math.radians(p_ax)
			iax = math.radians(i_ax)
			tax = math.radians(t_ax)



			a = math.sin(iax)**2
			b = math.sin(pax)**2
			c = math.sin(tax)**2
			#print a,b,c
			#p = numpy.array([a,b,c])
			#all_e_ch.append(p)



			string_a = str(a)+" "+str(b)+" "+str(c)+"\n"
			outfile.write(string_a)
			
			#num = array.array([a, b, c])
			#x_v.append(num)

			#ctr = math.radians(35.26)
			#pax, iax, tax = 35.43333, 35.43333, 35.43333

			#phi = math.atan(math.sin(tax)/math.sin(pax)) - math.radians(45)

			#x = (math.cos(iax)*math.sin(phi))/(math.sin(ctr)*math.sin(iax) + math.cos(ctr)*math.sin(iax)*math.cos(phi))
			#y = (math.cos(ctr)*math.sin(iax) - math.sin(ctr)*math.cos(iax)*math.cos(phi)) / (math.sin(ctr)*math.sin(iax) + math.cos(ctr)*math.sin(iax)*math.cos(phi))


			#if (x > -50 and x < 50 and y > - 50 and y < 50):
			#	x_v.append(x)
			#	y_v.append(y)

			#print x, y 

	#outfile = open("character.txt", 'w')
	#for i in x_v:
	#	outfile.write(str(i)+",")
	#outfile.close()

	#outfile = open("coords_y.txt", 'w')
	#for i in y_v:
	#	outfile.write(str(i)+",")
	#outfile.close()
	
	#plt.xlabel("Character")
	#plt.ylabel("Character")
        #plt.scatter(x_v, y_v)
	#plt.title(self.name+" triangle plot")
	#plt.show()	
	#plt.savefig(self.name+"_triangle_plot.png")
	#plt.clf()


	### USE THIS NOW ###
	#data = numpy.vstack((all_e_ch))
	#h = TernaryPlot(data)
	#h.plot()
	#h.save(self.name+"_triangle_plot.png")
	outfile.close()
	delete_mttk = subprocess.Popen("rm eq.txt", shell=True).wait()
        
    def return_m0_sum(self):
        
        return self.sum_list[len(self.sum_list)-1]
    
    def return_Sums(self):
        
        return self.sum_list[0:len(self.sum_list)-1]

    def create_strain_file(self):
	
	if (os.path.exists(self.name+"_strain.xy") and os.path.exists(self.name+"_strain_1.xy") and os.path.exists(self.name+"_strain_2.xy")):
		#print "Exists!"
		infile = open(self.name+"_strain.xy")
		for line in infile:
			splt = line.split()
			eig1 = float(splt[2])
			eig2 = float(splt[3])
			azim = float(splt[4])
		infile.close()
		return [eig1, eig2, azim]

	shear = 3*10**10
	t = 39
	denom = self.length*self.width*2*shear*t

	eig1, eig2, azim, expo = None, None, None, None

	infile = open(self.name+"_deltas.txt")
	for line in infile:
		splt = line.split()
		if len(splt) == 3:
			eig1 = float(splt[0])
			eig2 = float(splt[2])
		if len(splt) == 1 and azim == None:
			azim = float(splt[0])
			continue
		if len(splt) == 1 and azim != None and expo == None:
			expo = float(splt[0])
	infile.close()

	#print eig1, eig2, azim, expo

	if (expo != None and denom != None):
		if (eig1 != None):
			eig1 = (eig1*10**expo)/denom
		if (eig2 != None):
			eig2 = (eig2*10**expo)/denom

	#self.strain_data = [eig1, eig2, azim, expo]
	#print self.strain_data

	lon_t, lat_t = 0, 0

	for i in self.coords:
		lon_t += i[0]
		lat_t += i[1]

	lon = lon_t/len(self.coords)
	lat = lat_t/len(self.coords)

	if (eig1 != None and eig2 != None and azim != None and expo != None):
		outfile = open(self.name+"_strain.xy", 'w')
		outfile.write(str(lon)+" "+str(lat)+" "+str("{:f}".format(eig1))+" "+str("{:f}".format(eig2))+" "+str(azim))
		outfile.close()

		outfile = open(self.name+"_strain_1.xy", 'w')
		outfile.write(str(lon)+" "+str(lat)+" 0 "+str("{:f}".format(eig2))+" "+str(azim))
		outfile.close()

		outfile = open(self.name+"_strain_2.xy", 'w')
		outfile.write(str(lon)+" "+str(lat)+" "+str("{:f}".format(eig1))+" 0 "+str(azim))
		outfile.close()

	return  [eig1, eig2, azim]

    def create_Summation_file(self, *name):

	#if (os.path.exists(self.name+"_strain.xy")):
	#	print "Exists!"
	#	pass
	#else:
	#print "Creating file!"
	#self.create_strain_file()

	#if (os.path.exists(self.name+"_bplot.png")):
	#	pass
	#else:


	##self.return_b_value(0)
	##
	##if (os.path.exists(self.name+"_histogram.png")):
	##	pass
	##else:
	##	self.return_histogram()

	
	#self.return_histogram()
	#self.return_b_value(0)

	#if (os.path.exists(self.name+".xy") and os.path.exists(self.name+"_deltas.txt") and os.path.exists(self.name+"_coords.xy")):
	#	return

	

	if name:
		typefile = name[0]+".xy"
	else:
		typefile = raw_input('What do you want to save the data as [.txt/.XY]:')
	
	coords = self.coords
	
	lon_total = 0
	lat_total = 0

	outf = open(self.name+"_coords.xy", 'w')
	
	first = None

	for i in coords:
		#c = coords[i]
		lon_total = lon_total + i[0]
		lat_total = lat_total + i[1]
		write_s = str(i[0])+" "+str(i[1])+"\n"
		outf.write(write_s)
		if first == None:
			first = write_s
	outf.write(first)
		
	outf.close()

	lon = lon_total/len(coords)
	lat = lat_total/len(coords)

	self.av_coords = [lon, lat]

	#exp = 10**(int(math.log10(self.sum_list[0])))
	#exp = int(math.log10(self.sum_list[0]))
	#print "Exp is: "+str(math.log10(exp))
	#print self.sum_list[0]
	a = Decimal(self.sum_list[0]).normalize().as_tuple()
	
	exp = 10**(len(a.digits)-1 + int(a.exponent))
	#print exp

	m1 = self.sum_list[0]/exp
	m2 = self.sum_list[1]/exp
	m3 = self.sum_list[2]/exp
	m4 = self.sum_list[3]/exp
	m5 = self.sum_list[4]/exp
	m6 = self.sum_list[5]/exp

	#print m1,m2,m3,m4,m5,m6

	outfile = open(typefile, 'w')
	string_m = str(m1)+" "+str(m2)+" "+str(m3)+" "+str(m4)+" "+str(m5)+" "+str(m6)

	string_sum = str(lon)+" "+str(lat)+" "+str(self.depth)+" "+string_m+" "+str(math.log10(exp))+" X Y"
	
	outfile.write(string_sum)

	outfile.close()

	#print "get_region_delta.sh "+str(math.log10(exp))+" "+string_m+" "+self.name+"_deltas.txt"
	create_mttk = subprocess.Popen("bash/get_region_delta.sh "+str(int(math.log10(exp)))+" "+string_m+" "+self.name+"_deltas.txt", shell=True).wait()
    
    def do_Summation(self):
        
        totalmrr, totalmtt, totalmpp, totalmrt, totalmrp, totalmtp, totalm0 = 0, 0, 0, 0, 0, 0, 0
        
        for e in self.earthquakes:
            expo = 10**int(e.iexp)
            
            totalmrr += (10**-7 * expo * (e.mrr))
            totalmtt += (10**-7 * expo * (e.mtt))
            totalmpp += (10**-7 * expo * (e.mpp))
            totalmrt += (10**-7 * expo * (e.mrt))
            totalmrp += (10**-7 * expo * (e.mrp))
            totalmtp += (10**-7 * expo * (e.mtp))
            
            spl = e.m0.split('E+')
            val = float(spl[0])
            exponen = 10**float(spl[1])
            
            m0_new = (10**-7)*val*exponen
            
            totalm0 += m0_new
        
        return numpy.asanyarray([totalmrr, totalmtt, totalmpp, totalmrt, totalmrp, totalmtp, totalm0])
    
    def output_File(self, *name):
        
        if name:
            typefile = name[0]
        else:
            typefile = raw_input('What do you want to save the data as [.txt/.XY]:')
        
        outfile = open(typefile, 'w')
        
        for e in self.earthquakes:
           outfile.write(e.return_Equake())

        outfile.close()

        return typefile
    
    def return_stat_Depth(self, *arg):
        
        if arg:
            method = arg[0]
        else:
            method = float(input("1 for average of earthquakes, 2 for median."))

        all_e = len(self.earthquakes)
        
        if (method == 1):

            total = 0

            for e in self.earthquakes:
                total += e.depth
            
            if (all_e == 0):
                #print "No earthquake data found, setting depth as 0!"
                return 0
            
            #print "Average is: "+str((total)/all_e)+"."
            return (total/all_e)
        
        elif (method == 2):
            
            if (all_e % 2 == 0):
                return self.earthquakes[all_e/2].depth
            else:
                half = all_e/2.0
                return (self.earthquakes[int(half-0.5)].depth+self.earthquakes[int(half+0.5)].depth)/2
	
	elif (method == 3):
		minim, maxim = None

		for e in self.earthquakes:
			if (minim == None):
				minim = e.depth
			if (maxim == None):
				maxim = e.depth
			if e.depth > maxim:
				maxim = e.depth
			if e.depth < minim:
				minim = e.depth

		return abs(minim-maxim)
     
    def return_Length(self):
        
        return self.return_Distance(self.coords[0][0], self.coords[0][1], self.coords[1][0], self.coords[1][1])
    
    def return_Width(self):
        
        return self.return_Distance(self.coords[1][0], self.coords[1][1], self.coords[2][0], self.coords[2][1])
            
    def return_Area(self):
        
        #print self.coords[0][0]
        p1 = self.return_Length()
        p2 = self.return_Width()
        #print p1,p2

	#print "Area of region is: "+str(p1*p2)        
        return p1*p2
            
    def calculate_Strain(self, area, depth):

	m0sum = self.return_m0_sum()        
        av_depth = depth*1000
        s_area = area
	shear = 3.3*10**10
	t = 39*365*24*60*60
        
       # print "Area is: "+str(s_area)+"."
        
        vol = av_depth * s_area 
	print vol
        
        koscons = 1./(2*shear*vol*t) * m0sum
        ret = koscons*(365*24*60*60*10**9)

	outfile = open('all_strains.txt', 'a')
	outfile.write(str(ret)+'\n')
	outfile.close()

        return ret
    
    def calculate_Velocity(self, length, depth, *arg):
        
        m0sum = self.return_m0_sum()
	#print "LLL is: "+str(lll)
	#length_now = self.return_Length()
        
        shear = 3.3*10**10
        time = 39*365*24*60*60
	depth_metres = depth*1000
	sums = self.return_Sums()
	
	#print "Width is: "+str(self.width)
	#print "Length is: "+str(self.length)

	#print depth_metres, self.depth

	if arg and arg[0] != None:
            	angle = arg[0]
        else:
		print "Using default angle value of 15 degrees!"
		angle = 15
            #angle = float(raw_input('Use what angle for fault?'))
       # print self.sum_list
	def degreestorad(number):
            return number * math.pi/180

        w = (depth_metres)/math.sin(degreestorad(angle))
       # print "Shear:"+str(shear)+" Length"+str(self.length)+" W: "+str(w)+" Time is: "+str(time)+" depth: "+str(depth_metres)+" m0 is: "+str(m0sum)+" angle is: "+str(angle)+"."
	#print sums
        return (1000*365*24*60*60)*((1.0/(shear * length * w * time)) * m0sum)
        
    def return_Distance(self, lon_1, lat_1, lon_2, lat_2):
        
        def degreestorad(number):
            return number * math.pi/180
        
        r = 6371000
        
        lon1 = degreestorad(lon_1)
        lat1 = degreestorad(lat_1)
        lon2 = degreestorad(lon_2)
        lat2 = degreestorad(lat_2)
        
        d_lon = lon2 - lon1
        d_lat = lat2 - lat1
        
        a = math.sin(d_lat / 2.)**2 + math.cos(lat1) * math.cos(lat2) * math.sin(d_lon / 2.)**2
        c = 2. * math.asin(math.sqrt(a))

        return c * r

    def get_Bearing(lon1, lat1, lon2, lat2):
	
	d_lon = lon1 - lon2

	s = math.cos(math.radians(lat2))*math.sin(math.radians(d_lon))
	c = math.cos(math.radians(lat1))*math.sin(math.radians(lat2)) - math.sin(math.radians(lat1))*math.cos(math.radians(lat2))*math.cos(math.radians(d_lon))

	return numpy.arctan2(s, c)

    def return_girard_area(self):
	
	n = len(self.coords)
	angles = numpy.empty(n)

	r = 6371000

	all_lon = []
	all_lat = []

	for d in self.coords:
		all_lon.append(d[0])
		all_lat.append(d[1])
	
	for i in range(n):
		pB1, pA, pB2 = numpy.roll(all_lat, i)[:3]
		LB1, LA, LB2 = numpy.roll(all_lon, i)[:3]

		#east
		b1 = self.get_Bearing(LA, pA, LB1, pB1)
		b2 = self.get_Bearing(LA, pA, LB2, pB2)
		
		#add angles and add the sum to the array
		angles[i] = numpy.arccos(math.cos(-b1)*math.cos(-b2) + math.sin(-b1)*math.sin(-b2))

	return (sum(angles)-(n-2)*numpy.pi)*r**2

    def return_histogram(self):

	all_e = []	
	for e in self.earthquakes:
		all_e.append(e.depth)

	binwidth = 2
	plt.hist(all_e, bins = range(int(min(all_e)), int(max(all_e)) + binwidth, binwidth))
	plt.xlabel("Depth (km)")
	plt.ylabel("Frequency")
	plt.title(self.name+" depth histogram")
	plt.savefig(self.name+"_histogram.png")
	plt.clf()
    
    def return_b_value(self, *arg):
        
        temp_mag = []
        temp_num = []
        
        #earth_data = dict([e.mag, e.mag] for e in self.earthquakes)
        
        ed = dict()
        
        for e in self.earthquakes:
            
            key = e.mag

	      
            if key in ed:
                ed[key] += 1
            else:
                ed[key] = 1
        
        oed = collections.OrderedDict(sorted(ed.items()))
        
	total = sum(v for v in oed.itervalues())
	print total
	yinter = numpy.log10(total)
	xinter = yinter/-1.0

	print xinter, yinter

	tnum = [0]

        x, y = [], []
        
        if arg:
            filt = arg[0]
        else:
            filt = input('Use magnitude values above what?')

        for k in oed:
            if (float(k) > filt):

		x.append(float(k))
                y.append(total - sum(tnum))

		tnum.append(float(oed[k]))
               
	outfile = open(self.name+"_bvalue_points.xy", 'w')
	for x1, y1 in zip(x, y):
		outfile.write(str(x1)+","+str(y1)+"\n")
	outfile.close()
 
	#print x,y
        logx, logy = numpy.log10(x), numpy.log10(y)
                
        plt.xticks(rotation=65)
        #plt.tight_layout()
        
        coef = numpy.polyfit(x, logy, 1)
        y_fit = numpy.poly1d(coef)

	try:
		grad, yint = y_fit
	except ValueError:
		grad, yint = 0, 0
            
	#print yint
	#values = []
	#for i in x:
	#	values.append(-1.0*i + yint)
	#plt.plot(x, values, 'k')
	
	#a = len(x)
	#N = 10**(a-7.5)
	#N2 = 10**(a-7.4)

	#y_fit2 = -1.0, yint

	#diff = N2 - N
	#print numpy.log10(diff)	

	#plt.ylim([-1.0,3.0])
	#plt.xlim([4.5, 8.0])
	#plt.plot([0, yinter], [xinter, 0])
	plt.xlabel("Magnitude")
	plt.ylabel("log10(frequency)")
        plt.scatter(x, logy)

	#print N, N2
	#plt.plot([7.5, numpy.log10(N)],[5.0, numpy.log10(N2)], 'red')

	plt.title(self.name+" b plot")
	#yx.set_rotation(0)
        plt.plot(x, y_fit(x), '-r', label = 'Gradient (b value) is: '+str(-grad))
	#plt.plot(x, y_fit2(x), '-b', label = 'b value as 1.0')
        plt.legend(loc='upper left')
        plt.savefig(self.name+'_bplot.png')
	plt.clf()
	return grad
	#plt.show()
	#return abs(minim-maxim)
     
    def return_Length(self):
        
        return self.return_Distance(self.coords[0][0], self.coords[0][1], self.coords[1][0], self.coords[1][1])
    
    def return_Width(self):
        
        return self.return_Distance(self.coords[1][0], self.coords[1][1], self.coords[2][0], self.coords[2][1])
            
    def return_Area(self):
        
        #print self.coords[0][0]
        p1 = self.return_Length()
        p2 = self.return_Width()
        #print p1,p2

	#print "Area of region is: "+str(p1*p2)        
        return p1*p2
            
    def calculate_Strain(self, area, depth):

	m0sum = self.return_m0_sum()        
        av_depth = depth*1000
        s_area = area
	shear = 3.3*10**10
	t = 39*365*24*60*60
        
       # print "Area is: "+str(s_area)+"."
        
        vol = av_depth * s_area 
	print vol
        
        koscons = 1./(2*shear*vol*t) * m0sum
        ret = koscons*(365*24*60*60*10**9)

	outfile = open('all_strains.txt', 'a')
	outfile.write(str(ret)+'\n')
	outfile.close()

        return ret
    
    def calculate_Velocity(self, length, depth, *arg):
        
        m0sum = self.return_m0_sum()
	#print "LLL is: "+str(lll)
	#length_now = self.return_Length()
        
        shear = 3.3*10**10
        time = 39*365*24*60*60
	depth_metres = depth*1000
	sums = self.return_Sums()
	
	#print "Width is: "+str(self.width)
	#print "Length is: "+str(self.length)

	#print depth_metres, self.depth

	if arg and arg[0] != None:
            	angle = arg[0]
        else:
		print "Using default angle value of 15 degrees!"
		angle = 15
            #angle = float(raw_input('Use what angle for fault?'))
       # print self.sum_list
	def degreestorad(number):
            return number * math.pi/180

        w = (depth_metres)/math.sin(degreestorad(angle))
       # print "Shear:"+str(shear)+" Length"+str(self.length)+" W: "+str(w)+" Time is: "+str(time)+" depth: "+str(depth_metres)+" m0 is: "+str(m0sum)+" angle is: "+str(angle)+"."
	#print sums
        return (1000*365*24*60*60)*((1.0/(shear * length * w * time)) * m0sum)
        
    def return_Distance(self, lon_1, lat_1, lon_2, lat_2):
        
        def degreestorad(number):
            return number * math.pi/180
        
        r = 6371000
        
        lon1 = degreestorad(lon_1)
        lat1 = degreestorad(lat_1)
        lon2 = degreestorad(lon_2)
        lat2 = degreestorad(lat_2)
        
        d_lon = lon2 - lon1
        d_lat = lat2 - lat1
        
        a = math.sin(d_lat / 2.)**2 + math.cos(lat1) * math.cos(lat2) * math.sin(d_lon / 2.)**2
        c = 2. * math.asin(math.sqrt(a))

        return c * r

    def get_Bearing(lon1, lat1, lon2, lat2):
	
	d_lon = lon1 - lon2

	s = math.cos(math.radians(lat2))*math.sin(math.radians(d_lon))
	c = math.cos(math.radians(lat1))*math.sin(math.radians(lat2)) - math.sin(math.radians(lat1))*math.cos(math.radians(lat2))*math.cos(math.radians(d_lon))

	return numpy.arctan2(s, c)

    def return_girard_area(self):
	
	n = len(self.coords)
	angles = numpy.empty(n)

	r = 6371000

	all_lon = []
	all_lat = []

	for d in self.coords:
		all_lon.append(d[0])
		all_lat.append(d[1])
	
	for i in range(n):
		pB1, pA, pB2 = numpy.roll(all_lat, i)[:3]
		LB1, LA, LB2 = numpy.roll(all_lon, i)[:3]

		#east
		b1 = self.get_Bearing(LA, pA, LB1, pB1)
		b2 = self.get_Bearing(LA, pA, LB2, pB2)
		
		#add angles and add the sum to the array
		angles[i] = numpy.arccos(math.cos(-b1)*math.cos(-b2) + math.sin(-b1)*math.sin(-b2))

	return (sum(angles)-(n-2)*numpy.pi)*r**2

    def return_histogram(self):

	all_e = []	
	for e in self.earthquakes:
		all_e.append(e.depth)

	binwidth = 2
	plt.hist(all_e, bins = range(int(min(all_e)), int(max(all_e)) + binwidth, binwidth))
	plt.xlabel("Depth (km)")
	plt.ylabel("Frequency")
	plt.title(self.name+" depth histogram")
	plt.savefig(self.name+"_histogram.png")
	plt.clf()
    
    def return_b_value(self, *arg):
        
        temp_mag = []
        temp_num = []
        
        #earth_data = dict([e.mag, e.mag] for e in self.earthquakes)
        
        ed = dict()
        
        for e in self.earthquakes:
            
            key = e.mag

	      
            if key in ed:
                ed[key] += 1
            else:
                ed[key] = 1
        
        oed = collections.OrderedDict(sorted(ed.items()))
        
	total = sum(v for v in oed.itervalues())
	print total
	yinter = numpy.log10(total)
	xinter = yinter/-1.0

	print xinter, yinter

	tnum = [0]

        x, y = [], []
        
        if arg:
            filt = arg[0]
        else:
            filt = input('Use magnitude values above what?')

        for k in oed:
            if (float(k) > filt):

		x.append(float(k))
                y.append(total - sum(tnum))

		tnum.append(float(oed[k]))
               
	outfile = open(self.name+"_bvalue_points.xy", 'w')
	for x1, y1 in zip(x, y):
		outfile.write(str(x1)+","+str(y1)+"\n")
	outfile.close()
 
	#print x,y
        logx, logy = numpy.log10(x), numpy.log10(y)
                
        plt.xticks(rotation=65)
        #plt.tight_layout()
        
        coef = numpy.polyfit(x, logy, 1)
        y_fit = numpy.poly1d(coef)

	try:
		grad, yint = y_fit
	except ValueError:
		grad, yint = 0, 0
            
	#print yint
	#values = []
	#for i in x:
	#	values.append(-1.0*i + yint)
	#plt.plot(x, values, 'k')
	
	#a = len(x)
	#N = 10**(a-7.5)
	#N2 = 10**(a-7.4)

	#y_fit2 = -1.0, yint

	#diff = N2 - N
	#print numpy.log10(diff)	

	#plt.ylim([-1.0,3.0])
	#plt.xlim([4.5, 8.0])
	#plt.plot([0, yinter], [xinter, 0])
	plt.xlabel("Magnitude")
	plt.ylabel("log10(frequency)")
        plt.scatter(x, logy)

	#print N, N2
	#plt.plot([7.5, numpy.log10(N)],[5.0, numpy.log10(N2)], 'red')

	plt.title(self.name+" b plot")
	#yx.set_rotation(0)
        plt.plot(x, y_fit(x), '-r', label = 'Gradient (b value) is: '+str(-grad))
	#plt.plot(x, y_fit2(x), '-b', label = 'b value as 1.0')
        plt.legend(loc='upper left')
        plt.savefig(self.name+'_bplot.png')
	plt.clf()
	return grad
	#plt.show()
