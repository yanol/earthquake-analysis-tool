#Earthquake class for eq object

class Equake:

    def __init__(self, lon, lat, depth, mrr, mtt, mpp, mrt, mrp, mtp, iexp, mag, m0):
        self.lon = lon
        self.lat = lat
        self.depth = depth
        self.mrr = mrr
        self.mtt = mtt
        self.mpp = mpp
        self.mrt = mrt
        self.mrp = mrp
        self.mtp = mtp
        self.iexp = iexp
        self.mag = mag
        self.m0 = m0
	self.dips = None
        
    def return_Equake(self):
        
	#create string with all eq attributes

        s = ""
        
        s += str(self.lon)+" "+str(self.lat)+" "+str(self.depth)+" "+str(self.mrr)+" "+str(self.mtt)+" "+str(self.mpp)+" "+str(self.mrt)+" "+str(self.mrp)+" "+str(self.mtp)+" "+str(self.iexp)+" X Y "+str(self.mag)+" "+str(self.m0)+'\n'
        
        return s
