from equake import Equake
import matplotlib.path as mplPath
import array
import numpy
from region import Region

############################################################################################### MAIN DATAHANDLER CODE #######################################################################################        
#############################################################################################################################################################################################################
        
#data handler class to take .txt file and handle/create regions

class dataHandler:
    
    def __init__(self, filename):
        self.filename = filename
        self.all_earthquakes = self.parse_file()
	self.all_regions = []
    
    def parse_file(self):
        infile = open(self.filename)
        temp_list = []
        
        maxlon, maxlat, minlon, minlat = -300,-300,+300,+300
        
	#go through line by line and initiate all the earthquakes in the file as objects and return them to the handler

        for line in infile:
            lon, lat, depth, mrr, mtt, mpp, mrt, mrp, mtp, iexp, X, Y, name, mag, m0 = line.split()
            e = Equake(float(lon), float(lat), float(depth), float(mrr), float(mtt), float(mpp), float(mrt), float(mrp), float(mtp), int(iexp), str(mag), str(m0))
            
            #print lon,lat
            
            if (lon > maxlon):
                maxlon = lon
            if (float(lon) < minlon):
                minlon = lon
            if (lat > maxlat):
                maxlat = lat
            if (float(lat) < minlat):
                minlat = lat
            
            temp_list.append(e)
            
        print "We have generated " + str(len(temp_list)) + " earthquake objects."
        
        #print maxlon, maxlat, minlon, minlat
        #sort the eqs by their depth, so we have the most shallow at the beginning
        temp_list.sort(key = lambda x: x.depth, reverse=False)
        print "Deepest earthquake is at " + str(temp_list[len(temp_list) - 1].depth) + "."
        
        #whole_world = Region('whole_region',[[float(maxlon),float(minlat)],[float(maxlon),float(maxlat)],[float(minlon),float(maxlat)],[float(minlon),float(minlat)]], temp_list)
        
        return temp_list
    
        infile.close()

    #filter by magnitude
    def filter_by_magnitude(self, minim, maxim, *name):
	
	temp_list = []

	if (name):
		name_file = name[0]
	else:
		name_file = input('Enter the name of the file:')

	outfile = open(name_file, 'w')
        
        if (minim > maxim):
            minim, maxim = maxim, minim

	print "Maximum, minimum: "+str(maxim)+" "+str(minim)
            
        for e in self.all_earthquakes:
            
            if (e.mag > maxim):
                continue
                
            if (minim < e.mag):
		outfile.write(e.return_Equake())
                temp_list.append(e)
                
	outfile.close()
	print "Amount of earthquakes filtered is: "+str(len(temp_list))+"."
        return temp_list

    #function to filter ALL our earthquakes and create a file between the 2 depths set (e.q. 10-20km deep eqs)
    def filter_by_Depth(self, minim, maxim, *name):
        
        temp_list = []

	if (name):
		name_file = name[0]
	else:
		name_file = input('Enter the name of the file:')

	outfile = open(name_file, 'w')
        
        if (minim > maxim):
            minim, maxim = maxim, minim

	print "Maximum, minimum: "+str(maxim)+" "+str(minim)
            
        for e in self.all_earthquakes:
            
            if (e.depth > maxim):
                continue
                
            if (minim < e.depth):
		outfile.write(e.return_Equake())
                temp_list.append(e)
                
	outfile.close()
	print "Amount of earthquakes filtered is: "+str(len(temp_list))+"."
        return temp_list

    #function to create a region by giving 4 points for a polygon area to bound the eqs. Will filter and add any earthquakes from main handler which stores all earthquakes
    def create_Region(self, lon1, lat1, lon2, lat2, lon3, lat3, lon4, lat4, *name):

	p1 = [lon1, lat1]
	p2 = [lon2, lat2]
	p3 = [lon3, lat3] 
	p4 = [lon4, lat4]
        
	if name:
            name_string = name[0]
        else:
            name_string = input('Enter the name of the subregion:')

        temp_list = []
        
        area = [p1, p2, p3, p4]
        
        path = mplPath.Path(numpy.array((area[0], area[1], area[2], area[3]), dtype = float))
        
	#if (name_string == "reguion_3"):
	#outfile = open("region_3_filtered_eqs.txt", 'a')

        for e in self.all_earthquakes:
            if path.contains_point((e.lon, e.lat)):
		temp_list.append(e)
		#if (name_string == "region_3"):
		##	if (float(e.mag) < 6.50):
		#		temp_list.append(e)
		#	elif (float(e.mag) > 6.50):
		#		outfile.write(e.return_Equake())
		#else:
                #	temp_list.append(e)
	       
	#if (name_string == "reguion_3"):
	#outfile.close()

        new_r = Region(name_string, [p1, p2, p3, p4], temp_list)
        
        self.all_regions.append(new_r)

	print "Added new region. Number of earthquakes in it: "+str(len(temp_list))+". Region name: "+name_string+". Total number of regions is: "+str(len(self.all_regions))
        
    def return_main_Region(self):
        
        if self.all_regions[0]:
            return self.all_regions[0]
