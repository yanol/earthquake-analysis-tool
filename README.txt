# READ ME #

## Description ##

The EAT software (earthquake analysis tool) allows you to analyse any tectonic region in the world using a GUI, using open-source earthquake data (and the respective tensors and size of each earthquake). It offers many forms of analysis, including strain rate, velocity (from moment tensors), 3d depth plots, scatter plots, cross sections, depth filtering and many others. It also offers the ability to save and load data. 

Although it is mainly written in python, it also uses shell scripts as well as GMT.

## Requirements ##

* Python 2.7
* Bash
* GMT

Two modes:

## Auto-GUI Selection ##

1.) Automated way of doing it using the graphical user interface (GUI) by running startgui.py

1.i) This will load all the earthquakes in the CMT catalogue stored in the all_data.txt file and allow the user to choose any region, where the m0 sum, velocity and strain will be calculated.

1.ii) Subregions can be then chosen by dragging along a section of the screen. This will create a red, filled in rectangle. This can be rotated by dragging the inside of it.

1.iii) Double clicking will initiate the region (add it to the array of regions) and the focal mechanism sum, strain, and velocity will be calculated. They are all stored in the class.

## Manual ##

2.) Initiate the earthquake file using:

scotia = dataHandler('all_data_added.txt')

(or any other earhtquake.txt or .xy file)

This will be set as the whole region (not the main region, just the handler of the area will store all the values for this).

2.i) Add a region:


by running:

 scotia.create_Region(lon1, lat1, lon2, lat2, lon3, lat3, lon4, lat4, *name)

Where the longs and lats are the 4 vertices of the region. The datahandler will then take these inputs and generate all the necessary figures and values for velocity, strain, sum and other related numbers.

To return these, you can run:

for i in scotia.all_regions:
	i.return_all_values()

This will return the dip, velocity and sumlist. Other values can be accessed directly from the class.

## Possible options - easy in the GUI ##

* Strain 
* Velocity
* Sum, sum over random earthquakes, iteration over region with changing length and width
* 3d contour, surface and scatter plot
* Also possible to plot gmt sections. 
* Filter between certain depths and replot strain.

These are all accessible by running the automated mode and (GUI) double clicking to create a region once a rectangle has been drawn, and then clicking the statistics in the top left corner of the screen.

The GUI can also export all, which depending on what you select to be true or false, can do strain maps, average maps, velocity plots, gmt sections, bounding boxes, and earthquake map (as well as region maps).

## Loading file ##

To load my data file, simply run startgui.py and click file then load and load the savefile.txt.
