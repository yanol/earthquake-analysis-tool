#!/usr/bin/env bash

name=$1

while read -r line
do
	#echo $line
	#name=$line
	if [[ $line == *"Dip of fault plane (delta)"* ]]
	then
		#echo "It's here"
		delta_line=$line
	fi
	if [[ $line == *"Eigenvalues"* ]]
	then
		#echo "It's here"
		#line_wanted=$line
		read line
		eigen_line=$line
	fi
	if [[ $line == *"Scalar moment"* ]]
	then
		#echo "It's here"
		scalar_line=$line
	fi
	if [[ $line == *"Dip and strike of P axis"* ]]
	then
		#echo "It's here"
		pangle_line=$line
	fi
	if [[ $line == *"Dip and strike of I axis"* ]]
	then
		#echo "It's here"
		iangle_line=$line
	fi
	if [[ $line == *"Dip and strike of T axis"* ]]
	then
		#echo "It's here"
		tangle_line=$line
	fi
	if [[ $line == *"Strike of fault plane"* ]]
	then
		#echo "It's here"
		fault_line=$line
	fi
	if [[ $line == *"Rake (lambda)"* ]]
	then
		#echo "It's here"
		rake_line=$line
	fi
done < $name

my_deltas=$(echo $delta_line  | cut -d':' -f2)

my_eigen=$(echo $eigen_line  | cut -d':' -f2)

my_pangle=$(echo $pangle_line  | cut -d':' -f2)
new_pangle=$(echo $my_pangle | cut -d '(' -f2 | cut -d ')' -f1)
new_pangle=$(echo $my_pangle | cut -d ',' -f2 | cut -d ')' -f1)
p_dip=$(echo $my_pangle | cut -d '(' -f2 | cut -d ',' -f1)

my_iangle=$(echo $iangle_line  | cut -d':' -f2)
i_dip=$(echo $my_iangle | cut -d '(' -f2 | cut -d ',' -f1)

my_tangle=$(echo $tangle_line  | cut -d':' -f2)
t_dip=$(echo $my_tangle | cut -d '(' -f2 | cut -d ',' -f1)

my_expon=$(echo $scalar_line  | cut -d 'E' -f2)
my_expon=$(echo $my_expon | cut -f1 -d ' ')

my_faults=$(echo $fault_line  | cut -d':' -f2)
#my_fault=$(echo $my_faults | cut -f1 -d ' ')

my_rakes=$(echo $rake_line  | cut -d':' -f2)
#my_rake=$(echo $my_rakes | cut -f1 -d ' ')

#echo $my_val

echo "fault plane dip" $my_deltas > $name
echo $my_eigen >> $name
echo $new_pangle >> $name
echo $my_expon >> $name
echo "pit axes" $p_dip $i_dip $t_dip >> $name
echo "fault plane strike" $my_faults >> $name
echo "fault plane rake" $my_rakes >> $name
