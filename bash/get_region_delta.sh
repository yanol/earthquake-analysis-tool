#!/usr/bin/env bash

exponent=$1
m1=$2
m2=$3
m3=$4
m4=$5
m5=$6
m6=$7
name=$8

mttk > $name <<END
$exponent
1
$m1 $m2 $m3 $m4 $m5 $m6
y
y
n
END

sh extract_region_delta.sh $name

#s = 'Dip of fault plane (delta)';
#name

#while read -r line
#do
#	#name=$line
#	if [[ $line == *"Dip of fault plane (delta)"* ]]
#	then
#		#echo "It's here"
#		line_wanted=$line
#	fi
#done < $name

#echo "Name read from the file - $line_wanted"
#my_val=$(echo $line_wanted  | cut -d':' -f2)
#echo $my_val

#for num in $my_val
#do
#	echo $num
#done

#echo $my_val > $name
